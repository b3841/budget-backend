package com.djadiv.budgetbackendv3.periods

import com.djadiv.budgetbackendv3.periods.dto.CreatePeriodDTO
import com.djadiv.budgetbackendv3.periods.dto.PeriodTableDTO
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/api/private/v1/periods")
class PeriodsRestController(
    private val periodsService: PeriodsService
) {

    @GetMapping
    fun table(): List<PeriodTableDTO> {
        return periodsService.findAll()
    }

    @GetMapping("/{id}")
    fun getOne(@PathVariable id: UUID): PeriodEntity {
        return periodsService.findById(id)
    }

    @PostMapping("/save")
    fun save(@RequestBody period: CreatePeriodDTO) {
        periodsService.save(period)
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: UUID){
        periodsService.delete(id)
    }

    @GetMapping("/current")
    fun getCurrent(): PeriodEntity? {
        return periodsService.getCurrentPeriod()
    }

    @GetMapping("/currentOrLatest")
    fun getCurrentOrLatest(): PeriodEntity? {
        return periodsService.getCurrentOrLatest()
    }
}