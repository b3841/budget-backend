package com.djadiv.budgetbackendv3.periods

import com.djadiv.budgetbackendv3.periods.dto.CreatePeriodDTO
import com.djadiv.budgetbackendv3.periods.dto.PeriodTableDTO
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.util.*
import com.djadiv.budgetbackendv3.errors.excepions.IllegalArgumentException
import com.djadiv.budgetbackendv3.movements.MovementRepository
import com.djadiv.budgetbackendv3.budgets.BudgetService
import com.djadiv.budgetbackendv3.utils.Formatter
import com.djadiv.budgetbackendv3.utils.format
import org.springframework.data.domain.Sort
import org.springframework.transaction.annotation.Transactional
import java.time.LocalDateTime

@Service
class PeriodsService(
    private val periodRepository: PeriodRepository,
    private val movementRepository: MovementRepository,
    private val budgetService: BudgetService
) {

    fun findAll(): List<PeriodTableDTO> {
        return periodRepository
            .findAll(Sort.by("startDate").descending())
            .take(10)
            .map { it.toDto() }
    }

    @Transactional
    fun save(createPeriodDTO: CreatePeriodDTO) {
        periodRepository.findInPeriodRange(createPeriodDTO.startDate, createPeriodDTO.endDate, createPeriodDTO.id)
            ?.let {
                throw IllegalArgumentException(
                    "Persisting period intersects with existing",
                    "Сохраняемый период пересекается с существующим ${it.startDate.format(Formatter.DD_MMM_HH_MIN)} " +
                            ":: ${it.endDate.format(Formatter.DD_MMM_HH_MIN)}"
                )
            }

        val periodEntity = PeriodEntity(
            id = createPeriodDTO.id ?: UUID.randomUUID(),
            name = createPeriodDTO.name,
            startDate = createPeriodDTO.startDate,
            endDate = createPeriodDTO.endDate,
            comment = createPeriodDTO.comment
        )
        periodRepository.save(periodEntity)
    }

    fun findById(id: UUID): PeriodEntity {
        return periodRepository.findByIdOrNull(id) ?: throw IllegalArgumentException("No period with id = $id")
    }

    fun delete(id: UUID) {
        if (movementRepository.findAllByPeriodId(id).isNotEmpty())
            throw IllegalArgumentException("Period has movements [id:$id]", "У периода есть движения")

        if (budgetService.findAllByPeriodId(id).isNotEmpty())
            throw IllegalArgumentException("Period has budgets [id:$id]", "У периода есть бюджеты")

        // Concurency
        // Что если в этот момент у периода появятся движения??
        periodRepository.deleteById(id)
    }

    fun getCurrentPeriod(): PeriodEntity? {
        return periodRepository.findCurrentPeriod()
    }

    fun getCurrentOrLatest(): PeriodEntity? {
        return getCurrentPeriod()
            ?: periodRepository
                .findAll(
                    Sort.by("startDate")
                        .descending()
                )
                .firstOrNull()
    }


    fun periodByDate(date: LocalDateTime) =
        periodRepository.findPeriodByDate(date)


//    fun periodByDateOrException(date: LocalDate): PeriodEntity {
//        return periodRepository.findPeriodByDate(date)
//            ?: throw IllegalArgumentException(
//                "There is no period with date $date",
//                "Указанная дата ${date.formatWithYear()} не входит ни в один период"
//            )
//    }


}