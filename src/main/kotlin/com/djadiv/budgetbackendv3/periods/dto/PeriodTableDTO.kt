package com.djadiv.budgetbackendv3.periods.dto

import java.util.*

data class PeriodTableDTO(
    var id: UUID,
    var range: String,
    var name: String,
    var comment: String?
)
