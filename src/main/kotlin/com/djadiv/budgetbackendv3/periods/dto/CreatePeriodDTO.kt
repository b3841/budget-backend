package com.djadiv.budgetbackendv3.periods.dto

import org.springframework.format.annotation.DateTimeFormat
import java.time.LocalDateTime
import java.util.*
import jakarta.validation.constraints.NotEmpty

data class CreatePeriodDTO(

    var id: UUID? = null,

    @field:NotEmpty(message = "Укажите название периода")
    var name: String = "",

    @field:DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    var startDate: LocalDateTime = LocalDateTime.now(),

    @field:DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    var endDate: LocalDateTime = LocalDateTime.now(),

    var comment: String? = null

)