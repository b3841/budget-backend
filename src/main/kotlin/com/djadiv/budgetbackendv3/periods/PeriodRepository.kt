package com.djadiv.budgetbackendv3.periods

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.time.LocalDateTime
import java.util.*

@Repository
interface PeriodRepository : JpaRepository<PeriodEntity, UUID> {


    @Query(
        """
            SELECT a FROM PeriodEntity a
            WHERE 
                    ((a.startDate <= :startDate AND a.endDate >= :startDate)
                    OR (a.startDate <= :endDate AND a.endDate >= :endDate))
                    AND (CAST(:id as org.hibernate.type.UUIDCharType) IS NULL OR a.id != :id)
            """
    )
    fun findInPeriodRange(startDate: LocalDateTime, endDate: LocalDateTime, id: UUID?): PeriodEntity?

    @Query("SELECT a FROM PeriodEntity a WHERE a.startDate <= :today AND a.endDate >= :today")
    fun findCurrentPeriod(today: LocalDateTime = LocalDateTime.now()): PeriodEntity?

    @Query("SELECT a FROM PeriodEntity a WHERE a.startDate <= :date AND a.endDate >= :date")
    fun findPeriodByDate(date: LocalDateTime): PeriodEntity?
}