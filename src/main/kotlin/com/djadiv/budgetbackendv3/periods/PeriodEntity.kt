package com.djadiv.budgetbackendv3.periods

import com.djadiv.budgetbackendv3.periods.dto.PeriodTableDTO
import com.djadiv.budgetbackendv3.utils.Formatter
import com.djadiv.budgetbackendv3.utils.format
import java.time.LocalDateTime
import java.util.*
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table

@Entity
@Table(name = "periods")
data class PeriodEntity(

    @Id
    val id: UUID = UUID.randomUUID(),

    val name: String,

    @Column(name = "start_date")
    val startDate: LocalDateTime,

    @Column(name = "end_date")
    val endDate: LocalDateTime,

    val comment: String? = null
) {
    fun toDto() = PeriodTableDTO(
        id = id,
        range = startDate.format(Formatter.DD_MMM_HH_MIN)
                + " :: " + endDate.format(Formatter.DD_MMM_HH_MIN),
        name = name,
        comment = comment
    )

}
