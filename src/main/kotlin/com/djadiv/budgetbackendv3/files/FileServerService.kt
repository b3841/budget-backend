package com.djadiv.budgetbackendv3.files

import org.springframework.http.ResponseEntity
import org.springframework.web.multipart.MultipartFile

interface FileServerService {

    fun uploadFileToStore(file: MultipartFile): Attachment

    fun getFileFromStore(fileId: String): ResponseEntity<ByteArray>
}


