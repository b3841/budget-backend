package com.djadiv.budgetbackendv3.files

import org.hibernate.annotations.Type
import jakarta.persistence.*

@Entity
@Table(name = "movement_files")
data class MovementFileEntity (

    @Id
    @Column(name = "file_id")
     val fileId: String,
    @Column(name = "file_name")
     val fileName: String,

    @Lob
//    @Type(type = "org.hibernate.type.BinaryType")
     val data: ByteArray


    )