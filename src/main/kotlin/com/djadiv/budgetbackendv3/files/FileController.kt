package com.djadiv.budgetbackendv3.files

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile

@RestController
@RequestMapping("/api/private/v1/files")
class FileController(
    private val fileServerService: FileServerService
) {

    @PostMapping
    fun uploadFile(@RequestParam("file") file: MultipartFile): Attachment {
        return fileServerService.uploadFileToStore(file)
    }

    @GetMapping("/{fileId}")
    fun getFile(@PathVariable fileId: String ): ResponseEntity<ByteArray> {
        return fileServerService.getFileFromStore(fileId)
    }

}