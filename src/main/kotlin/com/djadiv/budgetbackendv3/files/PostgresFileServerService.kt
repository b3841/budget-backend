package com.djadiv.budgetbackendv3.files

import com.djadiv.budgetbackendv3.errors.excepions.FileUploadException
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.io.IOException
import java.util.*

@Service
class PostgresFileServerService(
    private val repository: MovementFilesRepository
) : FileServerService {
    override fun uploadFileToStore(file: MultipartFile): Attachment {
        // TODO (check size, check mime-type)
        val fileId = UUID.randomUUID().toString()
        val fileName = file.originalFilename!!
        try {
            repository.save(MovementFileEntity(fileId, fileName, file.bytes))
        } catch (ex: IOException) {
            throw FileUploadException("Access error. Temporary store may failed", ex);
        }
        return Attachment(fileName, fileId)
    }

    override fun getFileFromStore(fileId: String): ResponseEntity<ByteArray> {

        val image = repository.getById(fileId)

        return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(image.data)
    }
}