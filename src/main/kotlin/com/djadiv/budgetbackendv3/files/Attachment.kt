package com.djadiv.budgetbackendv3.files

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue


private val mapper = jacksonObjectMapper()

const val EMPTY_ARRAY_STRING = "[]"

data class Attachment(
    val fileName: String,
    val fileId: String,
//    val mimeType: String,
//    val description: String?,
    // ID файла пиктограммы
    val thumbnailFileId: String? = null,
    // ID файла для основного отображения
//    val displayFileId: String? = null,
    @JsonInclude(JsonInclude.Include.NON_NULL)
    val hash: String? = null
) {
    companion object {
        fun parseArrayFromString(filesString: String?): MutableList<Attachment> {
            return filesString.orEmpty().let {
                if (it.isNotEmpty()) {
                    mapper.readValue(it)
                } else {
                    mutableListOf()
                }
            }
        }

        fun parseAttachmentFromString(filesString: String?): Attachment? {
            return filesString?.let {
                mapper.readValue(it)
            }
        }

        fun writeAsString(array: List<Attachment>): String {
            return mapper.writeValueAsString(array)
        }

        fun writeAsString(attachment: Attachment?): String {
            return mapper.writeValueAsString(attachment)
        }
    }
}

fun List<Attachment>.writeAsString(): String {
    return mapper.writeValueAsString(this)
}
