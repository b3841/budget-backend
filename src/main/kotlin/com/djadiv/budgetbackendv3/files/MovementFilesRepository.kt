package com.djadiv.budgetbackendv3.files

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface MovementFilesRepository: JpaRepository<MovementFileEntity, String>{

}