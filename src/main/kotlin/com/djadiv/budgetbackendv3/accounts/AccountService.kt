package com.djadiv.budgetbackendv3.accounts

import com.djadiv.budgetbackendv3.accounts.dto.AccountTableDTO
import com.djadiv.budgetbackendv3.accounts.dto.BalanceByAccountProjectionDTO
import com.djadiv.budgetbackendv3.accounts.dto.CreateAccountDTO
import com.djadiv.budgetbackendv3.enums.CurrencyEnum
import com.djadiv.budgetbackendv3.errors.excepions.IllegalArgumentException
import com.djadiv.budgetbackendv3.movements.MovementService
import com.djadiv.budgetbackendv3.transfers.TransferRepository
import com.djadiv.budgetbackendv3.utils.Formatter
import com.djadiv.budgetbackendv3.utils.format
import com.djadiv.budgetbackendv3.utils.subs
import com.djadiv.budgetbackendv3.utils.toWord
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.math.BigDecimal
import java.util.*

@Service
class AccountService(
    private val accountRepository: AccountRepository,
    private val movementService: MovementService,
    private val accountService2: AccountService2
) {

    @Transactional
    fun save(createAccountDTO: CreateAccountDTO) {

        if (isExistByNameAndCurrency(createAccountDTO.name, createAccountDTO.currency)) throw IllegalArgumentException(
            "Account with [name:${createAccountDTO.name}] and [currency:${createAccountDTO.currency}] already exists",
            "Счет с именем ${createAccountDTO.name} и валютой ${createAccountDTO.currency} сущеcтвует"
        )

        val existingAccount: AccountEntity? = createAccountDTO.id?.let { accountRepository.findByIdOrNull(it) }

        val account = existingAccount?.copy(
            name = createAccountDTO.name, currency = createAccountDTO.currency, isSavings = createAccountDTO.savings
        ) ?: AccountEntity(
            id = UUID.randomUUID(),
            name = createAccountDTO.name,
            currency = createAccountDTO.currency,
            isSavings = createAccountDTO.savings
        )

        accountRepository.save(account)
    }

    fun findAll(onlyDictionary: Boolean): List<AccountTableDTO> {
        val accounts = findAll()
        return if (onlyDictionary) accounts.map { it.toDto() }
        else {
            // Получить последние движения по счетам
            val lastMovements = movementService.getLastMovementsByAccountIds(accounts.map { it.id })

            // sender
            val senderMovementIds =
                lastMovements.filter { it.transfer }.filter { it.operationAmount < BigDecimal.ZERO }.map { it.id }
            val receiverMovementIds =
                lastMovements.filter { it.transfer }.filter { it.operationAmount > BigDecimal.ZERO }.map { it.id }

//            val transferDetails: List<TransferEntity> =
//                transferRepository.findByMovementIds(lastMovements.map { it.id })

            accounts.map { account ->
                val lastMovement = lastMovements.find { it.accountId == account.id }
                account.toDto(
                    balance = lastMovement?.balance ?: BigDecimal.ZERO, lastAction = lastAction(lastMovement)
                )
            }
        }
    }

    fun findAll() = accountRepository.findAllByHiddenIsFalse()

    private fun lastAction(
        lastOperation: BalanceByAccountProjectionDTO?
//        transferDetails: List<TransferEntity>
    ): String? {
        return lastOperation?.let { lastOperation ->

//            val transferDetailAsSource = transferDetails.find { it.sourceMovement.id == lastOperation.id }
//            val transferDetailAsTarget = transferDetails.find { it.targetMovement.id == lastOperation.id }

//            val transferInfo = if (lastOperation.transfer) {
//                transferDetailAsSource?.let {
//                    "Перевод на ${transferDetailAsSource.targetAccountName}"
//                } ?: transferDetailAsTarget?.let {
//                    "Перевод с ${transferDetailAsTarget.sourceAccountName}"
//                }
//            } else ""

            val date = lastOperation.date.toWord(Formatter.DD_MMM)
            val amount = lastOperation.operationAmount
            val comment = lastOperation.comment?.let {
                lastOperation.comment?.subs(24) + " "
            } ?: ""

//            val categoryName = if (lastOperation.transfer) transferInfo else lastOperation.categoryName.subs(24)
            val categoryName = lastOperation.categoryName.subs(24)
            "$date ${amount.format()} $comment[$categoryName]"
        }
    }

    fun isExistByNameAndCurrency(name: String, currency: CurrencyEnum): Boolean {
        return accountRepository.existsByNameAndCurrency(name, currency)
    }

    fun findById(id: UUID) = accountService2.findById(id)

    fun delete(id: UUID) {
        if (movementService.findAllByAccountId(id)
                .isNotEmpty()
        ) throw IllegalArgumentException("Account has movements [id: $id]", "На счету есть движения")

        // TODO проверить что на данном счету нет переводов

        accountRepository.deleteById(id)
    }
}