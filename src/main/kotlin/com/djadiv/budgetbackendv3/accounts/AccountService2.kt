package com.djadiv.budgetbackendv3.accounts

import com.djadiv.budgetbackendv3.errors.excepions.IllegalArgumentException
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.util.*

@Service
class AccountService2(
    private val accountRepository: AccountRepository,
) {

    fun findById(id: UUID) =
        accountRepository.findByIdOrNull(id) ?: throw IllegalArgumentException("No account with id = $id")
}