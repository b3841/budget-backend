package com.djadiv.budgetbackendv3.accounts.dto

import com.djadiv.budgetbackendv3.enums.CurrencyEnum
import java.util.*
import jakarta.validation.constraints.NotBlank

data class AccountTableDTO(

    var id: UUID,

    var name: String,

    var amount: String,

    var currency: CurrencyEnum,

    var lastAction: String = "",

    var isSavings: Boolean
)