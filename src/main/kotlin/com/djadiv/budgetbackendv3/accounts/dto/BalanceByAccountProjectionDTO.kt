package com.djadiv.budgetbackendv3.accounts.dto

import java.math.BigDecimal
import java.time.LocalDateTime
import java.util.*

interface BalanceByAccountProjectionDTO {
    val id: UUID
    val balance: BigDecimal
    val accountId: UUID
    val date: LocalDateTime
    val operationAmount: BigDecimal
    val comment: String?
    val categoryName: String
    val transfer: Boolean
}