package com.djadiv.budgetbackendv3.accounts.dto

import com.djadiv.budgetbackendv3.enums.CurrencyEnum
import java.util.*
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotEmpty
import jakarta.validation.constraints.NotNull


data class CreateAccountDTO(
    var id: UUID? = null,

    @field:NotEmpty(message = "Укажите название счета")
    var name: String = "",

    @field:NotEmpty( message = "Указание валюты обязательно")
    var currency: CurrencyEnum = CurrencyEnum.KZT,

    var savings: Boolean = false
)