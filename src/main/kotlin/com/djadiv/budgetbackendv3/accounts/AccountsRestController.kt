package com.djadiv.budgetbackendv3.accounts

import com.djadiv.budgetbackendv3.accounts.dto.AccountTableDTO
import com.djadiv.budgetbackendv3.accounts.dto.CreateAccountDTO
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/api/private/v1/accounts")
class AccountsRestController(
    val accountService: AccountService
) {

    @GetMapping
    fun table(@RequestParam dict: Boolean = false): List<AccountTableDTO> {
        return accountService.findAll(dict)
    }

    @GetMapping("/{id}")
    fun getOne(@PathVariable id: UUID): CreateAccountDTO {
        return accountService.findById(id).toCreateAccountDto()
    }

    @PostMapping("/save")
    fun save(@RequestBody account: CreateAccountDTO) {
        accountService.save(account)
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: UUID){
        accountService.delete(id)
    }



}