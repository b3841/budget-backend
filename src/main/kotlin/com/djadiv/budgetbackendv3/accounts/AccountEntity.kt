package com.djadiv.budgetbackendv3.accounts

import com.djadiv.budgetbackendv3.accounts.dto.AccountTableDTO
import com.djadiv.budgetbackendv3.accounts.dto.CreateAccountDTO
import com.djadiv.budgetbackendv3.enums.CurrencyEnum
import jakarta.persistence.*
import org.hibernate.annotations.Type
import java.math.BigDecimal
import java.util.*

@Entity
@Table(name = "accounts")
data class AccountEntity(

    @Id
//    @Type(type="pg-uuid") from spring boot 2.x javax.persistence
    val id: UUID = UUID.randomUUID(),

    val name: String,

    @Column(name = "currency")
    @Enumerated(EnumType.STRING)
    val currency: CurrencyEnum,

    @Column(name = "is_savings")
    val isSavings: Boolean = false,
    
    val hidden: Boolean = false

) {
    fun toDto(balance: BigDecimal = BigDecimal(0), lastAction: String? = null) = AccountTableDTO(
        id = id,
        name = name,
        amount = balance.toPlainString(),
        currency = currency,
        lastAction = lastAction ?: "",
        isSavings = isSavings
    )

    fun toCreateAccountDto() = CreateAccountDTO(
        id = id,
        name = name,
        currency = currency,
        savings = isSavings
    )

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AccountEntity

        if (id != other.id) return false
        if (name != other.name) return false
        if (currency != other.currency) return false
        if (isSavings != other.isSavings) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + currency.hashCode()
        result = 31 * result + isSavings.hashCode()
        return result
    }


}
