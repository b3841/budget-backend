package com.djadiv.budgetbackendv3.accounts

import com.djadiv.budgetbackendv3.enums.CurrencyEnum
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface AccountRepository: JpaRepository<AccountEntity, UUID> {


    @Query("SELECT count(a) > 0 FROM  AccountEntity  a WHERE LOWER(TRIM(a.name)) = LOWER(TRIM(:name)) AND a.currency = :currency ")
    fun existsByNameAndCurrency(name: String, currency: CurrencyEnum): Boolean

    // supposed to be main function to retrieve all the rows
    fun findAllByHiddenIsFalse(): List<AccountEntity>
}