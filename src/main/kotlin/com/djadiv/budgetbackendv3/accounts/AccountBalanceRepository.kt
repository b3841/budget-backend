//package com.djadiv.budgetbackendv3.accounts
//
//import com.djadiv.budgetbackendv3.accounts.entities.AccountBalanceEntity
//import com.djadiv.budgetbackendv3.accounts.entities.AccountBalanceEntityID
//import org.springframework.data.jpa.repository.JpaRepository
//import org.springframework.data.jpa.repository.Query
//import org.springframework.stereotype.Repository
//import java.util.*
//
//@Repository
//interface AccountBalanceRepository : JpaRepository<AccountBalanceEntity, AccountBalanceEntityID> {
//
//
//    @Query(
//        """
//        SELECT a.* FROM account_balances a
//        WHERE a.account_id = :accountId
//        ORDER BY a.m_date DESC
//        LIMIT 1
//    """,
//        nativeQuery = true
//    )
//    fun findLastBalance(accountId: UUID): AccountBalanceEntity?
//
//
////    fun findFirstByAccId_AccountIdOrderByAccId_DateDesc(accountId: UUID): AccountBalanceEntity
//}