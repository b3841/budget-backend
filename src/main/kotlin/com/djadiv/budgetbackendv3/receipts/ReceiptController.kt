package com.djadiv.budgetbackendv3.receipts

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.net.URI
import java.util.*

@RestController
@RequestMapping("/api/private/v1/receipts")
class ReceiptController(
    private val service: ReceiptService
) {

    @PostMapping("/save")
    fun save(@RequestBody createReceipt: CreateReceiptDTO): ResponseEntity<Any> {
        return if (createReceipt.id == null) {
            val id = service.create(createReceipt)
            ResponseEntity.created(URI.create(id.toString())).build()
        } else {
            val updated = service.update(createReceipt)
            ResponseEntity.ok(updated)
        }
    }

    @GetMapping("/{id}")
    fun getOne(@PathVariable id: UUID) = service.getOne(id).toCreateDto()

}