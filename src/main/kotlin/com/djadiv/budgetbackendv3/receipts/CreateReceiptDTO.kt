package com.djadiv.budgetbackendv3.receipts

import com.djadiv.budgetbackendv3.enums.MovementTypeEnum
import com.djadiv.budgetbackendv3.movements.dto.CreateMovementDTO
import java.math.BigDecimal
import java.time.LocalDate
import java.time.LocalTime
import java.util.*
import jakarta.validation.constraints.NotEmpty

data class CreateReceiptDTO(

    var id: UUID? = null,

    @field:NotEmpty(message = "Укажите дату")
    val date: LocalDate, // store same value in movement

    @field:NotEmpty(message = "Укажите время")
    var time: LocalTime,  // store same value in movement
    
    val comment: String? = null,

    val location: String? = null,  // store same value in movement

    val fileIds: List<String> = emptyList(),
    
    val movements: List<CreateMovementDTO> = emptyList(),
    
    val total: BigDecimal? = null
)