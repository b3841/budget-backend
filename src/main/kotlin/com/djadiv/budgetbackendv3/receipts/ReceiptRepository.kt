package com.djadiv.budgetbackendv3.receipts

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.time.LocalDateTime
import java.util.UUID

@Repository
interface ReceiptRepository : JpaRepository<ReceiptEntity, UUID> {
    
    fun findByDate(targetDate: LocalDateTime) : ReceiptEntity?
    
}