package com.djadiv.budgetbackendv3.receipts

import com.djadiv.budgetbackendv3.errors.excepions.IllegalArgumentException
import com.djadiv.budgetbackendv3.movements.MovementEntity
import com.djadiv.budgetbackendv3.movements.MovementService
import com.djadiv.budgetbackendv3.movements.dto.CreateMovementDTO
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.math.BigDecimal
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.util.*

@Service
class ReceiptService(
    private val repository: ReceiptRepository,
    private val validator: ReceiptCreateValidator,
    private val movementService: MovementService
) {

    @Transactional
    fun create(createReceipt: CreateReceiptDTO): UUID {
        val (_, period) = validator.validate(createReceipt, null)
        val receiptId = UUID.randomUUID()
        
        // store receipt
        val targetDate = LocalDateTime.of(createReceipt.date, createReceipt.time)
        val receipt = ReceiptEntity(
            id = receiptId,
            amount = receiptTotal(createReceipt),
            date = targetDate,
            periodId = period.id,
            comment = createReceipt.comment,
            location = createReceipt.location,
            fileIds = createReceipt.fileIds.toString()
        )

        repository.save(receipt)

        // store movements
        createReceipt.movements.forEachIndexed { index, createMovementDTO ->
            movementService.create(
                makeCopyForPersist(createMovementDTO, createReceipt, index, receipt.id)
            )
        }
        
        return receiptId;
    }

    @Transactional
    fun update(createReceipt: CreateReceiptDTO): ReceiptEntity {
        val existingReceipt = getOne(createReceipt.id)
        val (_, period) = validator.validate(createReceipt, existingReceipt)

        val existingMovements = existingReceipt.movements
        val requestMovements = createReceipt.movements
        val existingMovsIds = existingMovements.map { it.id }.toSet()
        val requestMovsIds = requestMovements
            .mapNotNull { it.id }  // filter out with no id
            .toSet()

        // One or more movements added (create them)
        // Find new: save only movements w/o id
        requestMovements.filter { it.id == null }
            .forEachIndexed { index, createMovementDTO ->
                movementService.create(
                    makeCopyForPersist(createMovementDTO, createReceipt, index, existingReceipt.id)
                )
            }

        // One or more movements deleted (delete them)
        existingMovsIds
            // find deleted: mov's that are contained in existingMov's and are not in requestMov's
            .subtract(requestMovsIds)
            .forEach { movementService.delete(it) } // this triggers recalculate all balances after

        // One or more movements modified: amount, comment, photos (update them)
        existingMovsIds
            // find modified: mov's that are present in both existingMov's and requestMov's  
            .intersect(requestMovsIds)
            // we get collection of ids of movs that has been modified
            .forEachIndexed { index, changedMovId ->
                val createMovement = requestMovements
                    .find { requestMov -> requestMov.id == changedMovId }!!

                movementService.update(
                    makeCopyForPersist(createMovement, createReceipt, index, existingReceipt.id)
                )  // this also triggers recalculate 
            }

        // Receipt modified: account, location, date, comment, images
        val targetDate = LocalDateTime.of(createReceipt.date, createReceipt.time)
        val receipt = existingReceipt.copy(
            amount = receiptTotal(createReceipt),
            date = targetDate,
            periodId = period.id,
            comment = createReceipt.comment,
            location = createReceipt.location,
            fileIds = createReceipt.fileIds.toString(),
            movements = listOf()
        )

        return repository.save(receipt)
    }
    
    fun getOne(id: UUID?): ReceiptEntity {
        return repository.findByIdOrNull(id) ?: throw IllegalArgumentException("Receipt with id $id not found")
    }

    fun delete(id: UUID) {
        repository.findByIdOrNull(id) ?: return

        repository.deleteById(id)

        TODO()


    }

    private fun makeCopyForPersist(
        copyFrom: CreateMovementDTO,
        createReceipt: CreateReceiptDTO,
        index: Int,
        receiptEntityId: UUID
    ) = copyFrom.copy(
        date = createReceipt.date,
        time = createReceipt.time.plus(index.toLong(), ChronoUnit.SECONDS),
        store = createReceipt.location,
        receiptId = receiptEntityId,
        isTransfer = false
    )

    private fun receiptTotal(createReceipt: CreateReceiptDTO) =
        if (createReceipt.movements.isEmpty()) BigDecimal.ZERO
        else createReceipt.movements.sumOf { it.amount }

   

}