package com.djadiv.budgetbackendv3.receipts

import com.djadiv.budgetbackendv3.movements.MovementEntity
import com.djadiv.budgetbackendv3.periods.PeriodEntity
import java.math.BigDecimal
import java.time.LocalDateTime
import java.util.*
import jakarta.persistence.*
import org.hibernate.annotations.Fetch
import java.time.LocalDate
import java.time.LocalTime

@Entity
@Table(name = "receipts")
data class ReceiptEntity(

    @Id
    val id: UUID = UUID.randomUUID(),

    @Column(name = "amount")
    val amount: BigDecimal = BigDecimal.ZERO,

    @Column(name = "date")
    val date: LocalDateTime,

    @Column(name = "period_id")
    val periodId: UUID,

    @ManyToOne
    @JoinColumn(name = "period_id", insertable = false, updatable = false)
    val period: PeriodEntity? = null,

    val comment: String? = null,

    val location: String? = null,

    val fileIds: String? = null,

    @OneToMany(mappedBy = "receipt", fetch = FetchType.EAGER) // may cause N+1 problem with EAGER todo check
    val movements: List<MovementEntity> = emptyList()

) {
    fun toCreateDto() = CreateReceiptDTO(
        id = id,
        date = LocalDate.from(date),
        time = LocalTime.from(date),
        comment = comment,
        location = location,
        fileIds = emptyList(),
        total = amount,
        movements = movements.map { it.toCreateDto() }.sortedByDescending { LocalDateTime.of(it.date, it.time) }
    )
}
