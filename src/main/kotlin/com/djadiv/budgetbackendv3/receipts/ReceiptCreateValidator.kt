package com.djadiv.budgetbackendv3.receipts

import com.djadiv.budgetbackendv3.accounts.AccountEntity
import com.djadiv.budgetbackendv3.accounts.AccountService2
import com.djadiv.budgetbackendv3.categories.CategoryService
import com.djadiv.budgetbackendv3.enums.MovementTypeEnum
import com.djadiv.budgetbackendv3.errors.excepions.IllegalArgumentException
import com.djadiv.budgetbackendv3.movements.MovementEntity
import com.djadiv.budgetbackendv3.movements.dto.CreateMovementDTO
import com.djadiv.budgetbackendv3.periods.PeriodEntity
import com.djadiv.budgetbackendv3.periods.PeriodsService
import com.djadiv.budgetbackendv3.utils.formatWithYear
import com.djadiv.budgetbackendv3.utils.toLocalDateTimeStart
import org.springframework.stereotype.Component
import java.time.LocalDateTime
import kotlin.reflect.typeOf

@Component
class ReceiptCreateValidator(
    private val categoryService: CategoryService,
    private val periodsService: PeriodsService,
    private val accountService2: AccountService2,
    private val receiptRepository: ReceiptRepository
) {

    fun validate(
        createReceiptDTO: CreateReceiptDTO,
        existingReceipt: ReceiptEntity?
    ): Pair<AccountEntity?, PeriodEntity> {

        val existingMovements: List<MovementEntity>? = existingReceipt?.movements
        var account: AccountEntity? = null

        // Check period
        val date = createReceiptDTO.date.toLocalDateTimeStart()
        val period = periodsService.periodByDate(date)
            ?: throw IllegalArgumentException(
                "There is no period with date $date",
                "Указанная дата ${date.formatWithYear()} не входит ни в один период"
            )
        // Check same date already exists
        val targetDate = LocalDateTime.of(createReceiptDTO.date, createReceiptDTO.time)
        receiptRepository.findByDate(targetDate)
            ?.let {
                if (createReceiptDTO == null)
                    throw IllegalArgumentException(
                        "Receipt with the same datetime already exists $targetDate",
                        "Чек с такой датой и временем уже существует"
                    )
            }

        val movements = createReceiptDTO.movements

        if (movements.isNotEmpty()) {

            // Check all movements are type of OUTCOME
            val incomes = movements.filter { it.type == MovementTypeEnum.INCOME }
            if (incomes.isNotEmpty())
                throw IllegalArgumentException(
                    "Receipt ${createReceiptDTO.date}, ${createReceiptDTO.comment} has INCOMES $incomes",
                    "В чеке есть доходы. Функционал добавления доходов в чеке еще не реализован"
                )

            // Check all categories exist
            val categoryIds: Set<String> = movements.groupBy { it.categoryId }.keys
            if (categoryService.findByIds(categoryIds).size != categoryIds.size)
                throw IllegalArgumentException(
                    "Some category ids does not exist [$categoryIds]",
                    "Одна или более категории в чеке не существует"
                )

            // Check all accounts are the same
            val mapByAccount = movements.groupBy { it.accountId }
            if (mapByAccount.size > 1)
                throw IllegalArgumentException(
                    "One receipt cannot have more than one account ${mapByAccount.keys}",
                    "В одном чеке может быть указано не более одного счета"
                )

            // Check account exist
            account = accountService2.findById(mapByAccount.keys.first())

            // Check movement type didn't change
            if (existingMovements != null) {
                movements.forEach { createMovement ->
                    existingMovements
                        .find { existMov ->
                            existMov.id == createMovement.id
                                    && existMov.type != createMovement.type
                        }?.let {
                            throw IllegalArgumentException(
                                "Type of existing movement [id: ${it.id}], [type: ${it.type}]" +
                                        " does not match income parameter [type: ${createMovement.type}]"
                            )
                        }
                }
            }
        }

        return account to period
    }

}