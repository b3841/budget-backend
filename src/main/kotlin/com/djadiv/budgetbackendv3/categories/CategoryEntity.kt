package com.djadiv.budgetbackendv3.categories

import com.djadiv.budgetbackendv3.categories.dto.CategoryTableDTO
import com.djadiv.budgetbackendv3.categories.dto.CreateCategoryDTO
import com.djadiv.budgetbackendv3.enums.MovementTypeEnum
import java.util.*
import jakarta.persistence.*


@Entity
@Table(name = "categories")
data class CategoryEntity(

    @Id
    val id: String = UUID.randomUUID().toString(),

    val name: String,

    @Column(name = "parent_id")
    val parentId: String? = null,

    @ManyToOne
    @JoinColumn(name = "parent_id", insertable = false, updatable = false)
    val parent: CategoryEntity? = null,

    @Column(name = "is_storno")
    val isStorno: Boolean = false,

    @Transient
    val depth: Int = 0,

    @Enumerated(EnumType.STRING)
    val type: MovementTypeEnum
) {

    fun toDto() = CategoryTableDTO(
        id = id,
        name = name,
        formattedName = name,
        parentId = parentId,
        storno = isStorno,
        depth = depth
    )

    fun toCreateDto() = CreateCategoryDTO(
        id = id,
        name = name,
        parentId = parentId,
        type = type,
        storno = isStorno
    )
}

