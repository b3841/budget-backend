package com.djadiv.budgetbackendv3.categories.dto

import com.djadiv.budgetbackendv3.categories.dto.CategoryTableDTO

interface CategoryTableProjection {

    fun getId(): String

    fun getName(): String

    fun getParentId(): String?

    fun getFormattedName(): String

    fun getStorno(): Boolean

    fun getDepth(): Int

}


fun CategoryTableProjection.toTableDto(lastAction: String? = null) = CategoryTableDTO(
    id = getId(),
    name = getName(),
    formattedName = getFormattedName(),
    parentId = getParentId(),
    lastAction = lastAction,
    storno = getStorno(),
    depth = getDepth()
)
