package com.djadiv.budgetbackendv3.categories.dto

import com.djadiv.budgetbackendv3.enums.MovementTypeEnum
import java.util.*
import jakarta.validation.constraints.NotEmpty
import jakarta.validation.constraints.NotNull

data class CreateCategoryDTO(

    var id: String? = null,

    @field:NotEmpty(message = "Укажите название категории")
    var name: String = "",

    var parentId: String? = null,

    @field:NotNull(message = "Указание типа обязательно")
    var type: MovementTypeEnum = MovementTypeEnum.OUTCOME,

    var storno: Boolean = false

) {

}