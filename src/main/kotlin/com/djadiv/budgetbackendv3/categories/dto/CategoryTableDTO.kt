package com.djadiv.budgetbackendv3.categories.dto

import java.util.*

data class CategoryTableDTO(

    var id: String? = null,
    var name: String,
    var formattedName: String,
    var parentId: String? = null,
    var lastAction: String? = null,
    var storno: Boolean = false,
    var depth: Int = 0
)
