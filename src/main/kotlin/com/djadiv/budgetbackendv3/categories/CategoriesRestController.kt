package com.djadiv.budgetbackendv3.categories

import com.djadiv.budgetbackendv3.categories.dto.CategoryTableDTO
import com.djadiv.budgetbackendv3.categories.dto.CreateCategoryDTO
import com.djadiv.budgetbackendv3.enums.MovementTypeEnum
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/private/v1/categories")
class CategoriesRestController(
    private val categoryService: CategoryService,
) {

    @GetMapping
    fun table(
        @RequestParam type: MovementTypeEnum
    ): List<CategoryTableDTO> {
        return categoryService.findAll(type)
    }

    @GetMapping("/{id}")
    fun getOne(
        @PathVariable id: String,
        @RequestParam type: MovementTypeEnum
    ): CreateCategoryDTO {
        return categoryService.findById(id).toCreateDto()
    }

    @PostMapping("/save")
    fun save(@RequestBody category: CreateCategoryDTO) {
        categoryService.save(category)
    }

    @DeleteMapping("/{id}")
    fun delete(
        @PathVariable id: String,
        @RequestParam type: MovementTypeEnum
    ) {
        return categoryService.delete(id)
    }


}