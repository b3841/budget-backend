package com.djadiv.budgetbackendv3.categories

import com.djadiv.budgetbackendv3.categories.dto.CategoryTableDTO
import com.djadiv.budgetbackendv3.categories.dto.CategoryTableProjection
import com.djadiv.budgetbackendv3.categories.dto.CreateCategoryDTO
import com.djadiv.budgetbackendv3.categories.dto.toTableDto
import com.djadiv.budgetbackendv3.enums.MovementTypeEnum
import com.djadiv.budgetbackendv3.errors.excepions.IllegalArgumentException
import com.djadiv.budgetbackendv3.errors.excepions.ResourceNotFoundException
import com.djadiv.budgetbackendv3.movements.MovementRepository
import com.djadiv.budgetbackendv3.movements.dto.LastActionByCategoryProjection
import com.djadiv.budgetbackendv3.utils.Formatter
import com.djadiv.budgetbackendv3.utils.format
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.math.RoundingMode
import java.util.*

@Service
class CategoryService(
    private val movementRepository: MovementRepository,
    private val repository: CategoryRepository
) {

    fun save(createCategoryDTO: CreateCategoryDTO) {

        // проверка, что категория не может быть родителем самой себя
        if (createCategoryDTO.id != null && createCategoryDTO.id == createCategoryDTO.parentId)
            throw IllegalArgumentException(
                "Category cannot be parent of itself [id:${createCategoryDTO.id}]",
                "Категория не может быть родителем самой себя"
            )

        // проверка существования такого же имени под таким же родителем
        val parentId = createCategoryDTO.parentId
        val name = createCategoryDTO.name
        if (repository.existsByParentIdAndName(parentId.toString(), name, createCategoryDTO.id.toString()))
            throw IllegalArgumentException(
                "Category with the same name and parent already exist [parentId:$parentId, name: $name]",
                "Категория с таким именем и родителем уже существует"
            )

        repository.save(
            CategoryEntity(
                id = createCategoryDTO.id ?: UUID.randomUUID().toString(),
                name = createCategoryDTO.name,
                parentId = createCategoryDTO.parentId,
                isStorno = createCategoryDTO.storno,
                type = createCategoryDTO.type
            )
        )
    }

    fun findAll(type: MovementTypeEnum): List<CategoryTableDTO> {
        val categories = findAllRecursively(type)
        val lastActions = movementRepository.getLastMovementsByCategoryIds(categories.map { it.getId() }, type.type)

        return categories
            .map { category ->
                category.toTableDto(
                    getLastActionString(
                        lastActions.find {
                            it.getCategoryId() == category.getId()
                        })
                )
            }
    }

    fun findAllRecursively(type: MovementTypeEnum): List<CategoryTableProjection> {
        return repository.findAllRecursively(type.toString())
    }

    private fun getLastActionString(lastAction: LastActionByCategoryProjection?) =
        lastAction?.let {
            it.getComment() + " " + it.getOperationAmount().setScale(0, RoundingMode.DOWN) +
                    " :: " + it.getDate().format(Formatter.DD_MMM)
        }

    fun findById(id: String): CategoryEntity {
        return repository.findByIdOrNull(id) ?: throw ResourceNotFoundException("No categopry with id = $id")
    }

    fun delete(id: String) {
        repository.findById(id).ifPresent {
            if (movementRepository.findByCategoryId(id).isNotEmpty())
                throw IllegalArgumentException(
                    "Category has outcomes [id:$id], [name: ${it.name}]",
                    "У Категории есть движения: ${it.name}. "
                )

            if (repository.findByParentId(id).isNotEmpty())
                throw IllegalArgumentException(
                    "Category has childs [id:$id], [name: ${it.name}]",
                    "У Категории есть дочерние элементы: ${it.name}. "
                )

            repository.deleteById(id)
        }

    }

    fun findByIds(categoryIds: Set<String>): List<CategoryEntity> {
        return repository.findAllByIdIn(categoryIds)
    }


}