package com.djadiv.budgetbackendv3.categories

import com.djadiv.budgetbackendv3.categories.CategoryEntity
import com.djadiv.budgetbackendv3.categories.dto.CategoryTableProjection
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface CategoryRepository : JpaRepository<CategoryEntity, String> {


    fun findAllByIdIn(ids: Collection<String>) : List<CategoryEntity> // will it work with UUID?
    
    @Query(
        value = """WITH RECURSIVE r AS (
    SELECT id,
           parent_id,
           is_storno,
           name,
           name as formattedName,
           name AS path,
           1    AS depth
    FROM categories
    WHERE parent_id IS NULL
      AND type = :type

    UNION

    SELECT a.id,
           a.parent_id,
           a.is_storno,
           a.name,
           lpad(a.name, length(a.name) + r.depth * 2, '- ') AS formattedName,
           r.path || '->' || a.name                         AS path,
           r.depth + 1                                      AS depth
    FROM categories a
             JOIN r ON a.parent_id = r.id
        AND type = :type
)
SELECT r.id,
       r.parent_id AS parentId,
       r.formattedName,
       r.name,
       r.is_storno AS storno,
       r.depth
FROM r

ORDER BY path;""", nativeQuery = true
    )
    fun findAllRecursively(type: String): List<CategoryTableProjection>

    @Query(
        """SELECT COUNT(a.id) > 0 
            FROM categories a 
            WHERE a.parent_id = :parentId 
            AND LOWER(a.name) = LOWER(:name) 
            AND (:id IS NULL OR a.id != :id)""",
        nativeQuery = true
    )
    fun existsByParentIdAndName( parentId: String?, name: String, id: String?): Boolean

    fun findByParentId(id: String): List<CategoryEntity>
}