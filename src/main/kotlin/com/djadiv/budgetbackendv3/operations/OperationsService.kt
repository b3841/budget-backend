//package com.djadiv.budgetbackendv3.operations
//
//import com.djadiv.budgetbackendv3.movements.MovementEntity
//import com.djadiv.budgetbackendv3.transfers.TransferEntity
//import org.springframework.data.domain.PageRequest
//import org.springframework.data.domain.Sort
//import org.springframework.stereotype.Service
//import org.springframework.transaction.annotation.Propagation
//import org.springframework.transaction.annotation.Transactional
//import java.math.BigDecimal
//
//@Service
//class OperationsService(
//    private val operationsRepository: OperationsRepository
//) {
//
//    @Transactional
//    fun updateBalance(operation: OperationEntity) {
//        val lastBalance: OperationEntity? = operationsRepository.getLastBalance(
//            operation.accountId,
//            PageRequest.of(1, 1, Sort.by("date").descending())
//        )
//            .firstOrNull()
//
//        when (operation) {
//            is MovementEntity -> {
//                val newBalance = (lastBalance?.balance ?: BigDecimal.ZERO).plus(operation.operationAmount)
//                operationsRepository.save(operation.copy(balance = newBalance))
//            }
//            is TransferEntity ->{
//                val newBalance = (lastBalance?.balance ?: BigDecimal.ZERO).minus(operation.operationAmount)
//                operationsRepository.save(operation.copy(balance = newBalance))
//                val lastBalanceTarget: OperationEntity? = operationsRepository.getLastBalance(
//                    operation.targetId,
//                    PageRequest.of(1, 1, Sort.by("date").descending())
//                )
//                    .firstOrNull()
//                val newBalanceTarget = (lastBalanceTarget?.balance ?: BigDecimal.ZERO).plus(operation.operationAmount)
////                operationsRepository.save()
//
//
//            }
//        }
//
//    }
//}