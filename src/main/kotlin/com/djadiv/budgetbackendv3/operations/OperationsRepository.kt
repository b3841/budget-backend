//package com.djadiv.budgetbackendv3.operations
//
//import org.springframework.data.domain.PageRequest
//import org.springframework.data.domain.Pageable
//import org.springframework.data.domain.Sort
//import org.springframework.data.jpa.repository.JpaRepository
//import org.springframework.data.jpa.repository.Query
//import org.springframework.stereotype.Repository
//import java.util.*
//
//@Repository
//interface OperationsRepository : JpaRepository<OperationEntity, UUID> {
//
//
//    @Query(
//        """SELECT a FROM OperationEntity a
//        WHERE a.accountId = :accountId
//    """
//    )
//    fun getLastBalance(accountId: UUID, page: Pageable = getPage()): List<OperationEntity>
//
//
//}