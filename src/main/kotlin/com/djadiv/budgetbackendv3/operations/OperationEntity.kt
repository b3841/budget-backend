//package com.djadiv.budgetbackendv3.operations
//
//import com.djadiv.budgetbackendv3.accounts.AccountEntity
//import com.djadiv.budgetbackendv3.periods.PeriodEntity
//import com.djadiv.budgetbackendv3.enums.OperationType
//import org.springframework.data.domain.PageRequest
//import org.springframework.data.domain.Sort
//import java.math.BigDecimal
//import java.time.LocalDateTime
//import java.util.*
//import jakarta.persistence.*
//
//
//fun getPage(): PageRequest = PageRequest.of(0, 1, Sort.by("date").descending())
//
//// Joined Subclass Inheritance Strategy
//@Entity
//@Table(name = "operations")
//@Inheritance(strategy = InheritanceType.JOINED)
//@DiscriminatorColumn(name = "operation_type")
//open class OperationEntity(
//
//    @Id
//    open val id: UUID = UUID.randomUUID(),
//
//    @Column(name = "account_id")
//    open val accountId: UUID,
//
//    @ManyToOne
//    @JoinColumn(name = "account_id", insertable = false, updatable = false)
//    open val account: AccountEntity? = null,
//
//    @Column(name = "m_date")
//    open val date: LocalDateTime,
//
//    @Column(name = "operation_amount")
//    open val operationAmount: BigDecimal = BigDecimal.ZERO,
//
//    open val comment: String? = null,
//
//    @Column(name = "period_id")
//    open val periodId: UUID,
//
//    @ManyToOne
//    @JoinColumn(name = "period_id", insertable = false, updatable = false)
//    open val period: PeriodEntity? = null,
//
////    @Enumerated(EnumType.STRING)
////    @Column(name = "operation_type")
//    @Transient
//    open val operationType: OperationType,
//
//    open var balance: BigDecimal = BigDecimal.ZERO,
//) {
//}