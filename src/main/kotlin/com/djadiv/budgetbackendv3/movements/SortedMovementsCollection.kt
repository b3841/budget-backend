package com.djadiv.budgetbackendv3.movements

import java.math.BigDecimal
import java.time.temporal.ChronoUnit


// Для новых
//  - В середину SortedMovementsCollection
// Получить все движения после даты
// Новое движение вставить в начало. Получить коллекцию
// пересчитать даты с начала
// пересчитать балансы с начала
// сохранить всю коллекцию
//  - в конец
// посчитать баланс нового движения
// сохранить движение

// Для существующих
// Дата не изменилась
//  - Движение вперед
//  - - В середину ExistingForwardSortedMovementsCollection
// Получить все движения после исходной даты
// Вставить редактируемое движение в середину; Получить коллекцию
// Пересчитать даты после того места, где вставили редактируемое движение
// пересчитать балансы с начала
// сохранить коллекцию
//  - - в конец ExistingForwardToEndSortedMovementsCollection
// Получить все движения после исходной даты
// Вставить редактируемое движение в конец; Получить коллекцию
// пересчитать балансы с начала коллекции
// даты пересчитывать не нужно
// сохранить коллекцию
//  - Движение назад
//  - - В середину SortedMovementsCollection
// Получить все движения после новой даты
// Вставить редактируемое движение в начало; Получить коллекцию
// Пересчитать даты с начала
// Пересчитать балансы с начала
// Сохранить коллекцию
//  - - В конец
// Обновить редактируемое движение
// посчитать баланс
// Сохранить движение


open class SortedMovementsCollection(
    // движения, которые существуют в базе. они или после target даты, или после исходной даты
    var existingMovements: MutableList<MovementEntity>
) {
    // Вставить в начало коллекции
    open fun insertMovementByDate(newMovement: MovementEntity): SortedMovementsCollection {
        val newMovementsCollection = mutableListOf<MovementEntity>()
        newMovementsCollection.add(newMovement)
        newMovementsCollection.addAll(existingMovements)
        existingMovements = newMovementsCollection
        return this
    }

    // пересчитать даты с начала коллекции
    open fun recalculateDates(): SortedMovementsCollection {
        recalculateDatesAfter(this.existingMovements.first())
        return this
    }

    protected fun recalculateDatesAfter(next: MovementEntity) {
        // найти элемент в коллекции по дате next
        // изменить время у найденного элемента
        // найти элемент в коллекции по дате только что найденного элемента
        // если такой существует, то вызвать метод заново
        existingMovements.find { it.date == next.date && it.id != next.id }
            ?.let {
                val index = existingMovements.indexOf(it)
                val movmntWithTimeShifted = it.copy(date = it.date.plus(1, ChronoUnit.SECONDS))
                existingMovements[index] = movmntWithTimeShifted
                if (existingMovements.any { it.date == movmntWithTimeShifted.date && it.id != movmntWithTimeShifted.id })
                    recalculateDatesAfter(movmntWithTimeShifted)
            }
    }

    // пересчитать балансы с начала коллекции
    open fun recalculateBalances(lastBalance: BigDecimal): SortedMovementsCollection {
        var currentBalance = lastBalance;
        existingMovements.map {
            currentBalance += (it.operationAmount * it.type.sign)
            it.balance = currentBalance
            it
        }
        return this
    }

}