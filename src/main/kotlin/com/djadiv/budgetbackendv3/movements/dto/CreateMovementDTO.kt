package com.djadiv.budgetbackendv3.movements.dto

import com.djadiv.budgetbackendv3.files.Attachment
import com.djadiv.budgetbackendv3.enums.MovementTypeEnum
import java.math.BigDecimal
import java.time.LocalDate
import java.time.LocalTime
import java.util.*
import jakarta.validation.constraints.NotEmpty

data class CreateMovementDTO(

    var id: UUID? = null,

    @field:NotEmpty(message = "Укажите категорию")
    val categoryId: String,

    @field:NotEmpty(message = "Укажите счет")
    val accountId: UUID,

    @field:NotEmpty(message = "Укажите дату")
    val date: LocalDate,

    @field:NotEmpty(message = "Укажите время")
    var time: LocalTime,

    @field:NotEmpty(message = "Укажите сумму")
    val amount: BigDecimal, // Приходит без знака

    val balance: BigDecimal? = null, // for output param
    
    val comment: String? = null,

    @field:NotEmpty
    val type: MovementTypeEnum,

    val isTransfer: Boolean = false,

    val store: String? = null,

    val fileIds: List<String> = emptyList(),
    
    val receiptId: UUID? = null // for output param
)