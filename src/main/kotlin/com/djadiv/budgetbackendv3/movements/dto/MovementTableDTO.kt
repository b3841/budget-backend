package com.djadiv.budgetbackendv3.movements.dto

import com.djadiv.budgetbackendv3.enums.MovementTypeEnum
import java.math.BigDecimal
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.util.*
import jakarta.validation.constraints.NotEmpty

data class MovementTableDTO (

    var id: UUID? = null,
    val categoryId: String,
    val categoryName: String,
    val accountName: String,
    val accountId: UUID,
    val date: LocalDate,
    val time: LocalTime,
    val amount: BigDecimal,
    val balance: BigDecimal,
    val comment: String? = null,
    val type: MovementTypeEnum,
    val isTransfer: Boolean,
    val isTransferFee: Boolean,
    val transferId: UUID?,
    val store: String?,
    val receiptId: UUID?
)