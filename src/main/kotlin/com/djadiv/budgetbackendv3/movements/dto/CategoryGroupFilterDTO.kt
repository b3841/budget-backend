package com.djadiv.budgetbackendv3.movements.dto

import com.djadiv.budgetbackendv3.enums.RangeTypeEnum
import java.time.Instant
import java.time.LocalDate
import java.util.*

data class CategoryGroupFilterDTO(
    val rangeType: RangeTypeEnum,
    val startDate: LocalDate? = null,
    val endDate: LocalDate? = null,
    val periodId: UUID?,
    val accountId: UUID?
)