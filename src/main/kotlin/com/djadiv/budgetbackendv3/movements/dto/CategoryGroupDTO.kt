package com.djadiv.budgetbackendv3.movements.dto

import java.math.BigDecimal

data class CategoryGroupDTO(
    val categoryId: String,
    val parentCategoryId: String?,
    val depth: Int,
    val name: String,
    var own: BigDecimal,
    var child: BigDecimal = BigDecimal.ZERO
)

data class TotalByCategory(
    val categoryId: String,
    var amount: BigDecimal,
    var parentCategoryId: String?

    )