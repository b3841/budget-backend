package com.djadiv.budgetbackendv3.movements.dto

import java.math.BigDecimal
import java.time.LocalDateTime

interface LastActionByCategoryProjection {

    fun getCategoryId(): String

    fun getDate(): LocalDateTime

    fun getOperationAmount(): BigDecimal

    fun getComment(): String
}