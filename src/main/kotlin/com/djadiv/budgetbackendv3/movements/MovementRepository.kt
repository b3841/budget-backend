package com.djadiv.budgetbackendv3.movements

import com.djadiv.budgetbackendv3.accounts.dto.BalanceByAccountProjectionDTO
import com.djadiv.budgetbackendv3.periods.PeriodEntity
import com.djadiv.budgetbackendv3.enums.MovementTypeEnum
import com.djadiv.budgetbackendv3.movements.dto.LastActionByCategoryProjection
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.time.LocalDateTime
import java.util.*

@Repository
interface MovementRepository : JpaRepository<MovementEntity, UUID> {
    
    fun findByCategoryId(id: String): List<MovementEntity>

    @Query(
        """SELECT DISTINCT ON (category_id) category_id, m_date AS date, operation_amount, comment
                FROM movements a
                WHERE category_id IN (:categoryIds)
                AND type = :type
                ORDER BY category_id, m_date DESC;""",
        nativeQuery = true
    )
    fun getLastMovementsByCategoryIds(categoryIds: List<String>, type: String): List<LastActionByCategoryProjection>

    @Query(
        """
            SELECT a.* FROM movements a
            WHERE a.account_id = :accountId
            AND (:id IS NULL OR CAST(a.id AS VARCHAR) != :id)
            ORDER BY m_date DESC
            LIMIT 1
        """,
        nativeQuery = true
    )
    fun getLastMovementByAccountId(
        accountId: UUID,
        id: String?
    ): MovementEntity? // TODO Сделать версия с получением последнего движения до даты

    @Query(
        """
            SELECT a.* FROM movements a
            WHERE a.account_id = :accountId
            AND m_date < :date
            ORDER BY m_date DESC
            LIMIT 1
        """,
        nativeQuery = true
    )
    fun getLastMovementByAccountIdBeforeDate(
        accountId: UUID,
        date: LocalDateTime
    ): MovementEntity? // Написать кто пользуется этим ID

    @Query(
        """
            SELECT a.* FROM movements a
            WHERE a.account_id = :accountId
            AND m_date >= :date
            ORDER BY m_date
        """,
        nativeQuery = true
    )
    fun getAllMovementsByAccountIdAfterDate(accountId: UUID, date: LocalDateTime): List<MovementEntity>

    @Query(
        """SELECT DISTINCT ON (account_id) CAST(account_id AS VARCHAR) AS accountId,
                                CAST(a.id AS VARCHAR),
                                balance,
                                m_date                      AS date,
                                operation_amount            AS operationAmount,
                                a.comment,
                                c.name                      AS categoryName,
                                is_transfer                 AS transfer
FROM movements a
         LEFT JOIN categories c on a.category_id = c.id
WHERE account_id IN (:accountIds)
ORDER BY account_id, m_date DESC""",
        nativeQuery = true
    )
    fun getLastMovementsByAccountIds(accountIds: List<UUID>): List<BalanceByAccountProjectionDTO>

    fun findAllByPeriodAndTypeAndCategoryIdNotIn(
        currentPeriod: PeriodEntity, type: MovementTypeEnum, categoryIds: List<String>
    ): List<MovementEntity>

//    fun findAllByAccountId(id: UUID): List<MovementEntity>
    fun findAllByAccountId(id: UUID, sort: Sort = Sort.by("date").descending()): List<MovementEntity>

    fun findAllByTypeAndDateBetweenAndCategoryIdNotIn(
        type: MovementTypeEnum, start: LocalDateTime, end: LocalDateTime, categoryIds: List<String>
    ): List<MovementEntity>

    fun findAllByPeriodId(id: UUID): List<MovementEntity>

    @Query(
        """SELECT a FROM MovementEntity a 
                WHERE a.type = :type 
                AND a.categoryId = :categoryId
                AND (:startIsEmpty = TRUE OR a.date >= :start )
                AND (:endIsEmpty = TRUE OR a.date <= :end)
                AND (CAST(:periodId AS org.hibernate.type.UUIDCharType) IS NULL OR :periodId = a.periodId)
                AND (CAST(:accountId AS org.hibernate.type.UUIDCharType) IS NULL OR a.accountId = :accountId) 
                ORDER BY a.date 
                """
    )
    fun findByCategoryAndFilter(
        type: MovementTypeEnum,
        categoryId: String,
        start: LocalDateTime?,
        end: LocalDateTime?,
        periodId: UUID?,
        accountId: UUID?,
        startIsEmpty: Boolean,
        endIsEmpty: Boolean,
    ): List<MovementEntity>


    @Query(
        """SELECT a FROM MovementEntity a 
                WHERE (:startIsEmpty = TRUE OR a.date >= :start )
                AND (:endIsEmpty = TRUE OR a.date <= :end)
                AND (CAST(:accountId AS org.hibernate.type.UUIDCharType) IS NULL OR a.accountId = :accountId) 
                AND (CAST(:periodId AS org.hibernate.type.UUIDCharType) IS NULL OR :periodId = a.periodId)
                ORDER BY a.date DESC
                """
    )
    fun findAllByFilter(
        start: LocalDateTime?,
        end: LocalDateTime?,
        periodId: UUID?,
        accountId: UUID?,
        startIsEmpty: Boolean,
        endIsEmpty: Boolean,
    ): List<MovementEntity>


    @Query("""SELECT a.* FROM movements a
                WHERE (
                    SELECT count(b.*) FROM movements b
                                WHERE b.account_id = a.account_id
                                AND b.m_date = a.m_date
                    ) > 1
    """,
    nativeQuery = true)
    fun findAllTimestampDuplicates(): List<MovementEntity>

    @Query("SELECT max(date) FROM MovementEntity")
    fun lastDate(): LocalDateTime

}