package com.djadiv.budgetbackendv3.movements

import com.djadiv.budgetbackendv3.enums.MovementTypeEnum
import com.djadiv.budgetbackendv3.errors.excepions.IllegalArgumentException
import com.djadiv.budgetbackendv3.movements.dto.*
import com.djadiv.budgetbackendv3.categories.CategoryService
import com.djadiv.budgetbackendv3.periods.PeriodRepository
import com.djadiv.budgetbackendv3.settings.SettingsKey
import com.djadiv.budgetbackendv3.settings.SettingsService
import com.djadiv.budgetbackendv3.transfers.TransferEntity
import com.djadiv.budgetbackendv3.transfers.TransferRepository
import com.djadiv.budgetbackendv3.utils.extractDates
import com.djadiv.budgetbackendv3.utils.toLocalDateTimeEnd
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.math.BigDecimal
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*
import jakarta.persistence.EntityManager
import jakarta.persistence.PersistenceContext
import jakarta.persistence.criteria.CriteriaQuery
import jakarta.persistence.criteria.Root

@Service
class MovementService(
    private val categoryService: CategoryService,
    private val repository: MovementRepository,
    private val periodRepository: PeriodRepository,
    private val validator: MovementCreateValidator,
    private val settingsService: SettingsService,
    private val transferRepository: TransferRepository
) {
    @PersistenceContext
    lateinit var entityManager: EntityManager

    @Transactional
    fun create(createMovement: CreateMovementDTO): MovementEntity {
        val (_, period) = validate(createMovement, null)

        val amount = createMovement.amount // store without sign
        var targetDate = LocalDateTime.of(createMovement.date, createMovement.time)
        val accountId = createMovement.accountId

        val newMovement = MovementEntity(
            categoryId = createMovement.categoryId,
            accountId = accountId,
            periodId = period.id,
            date = targetDate,
            operationAmount = amount,
            comment = createMovement.comment,
            balance = amount,  // По-умолчанию равен сумме операции, в дальнейшем пересчитывается
            type = createMovement.type,
            isTransfer = createMovement.isTransfer,
            store = createMovement.store,
            fileIds = createMovement.fileIds.toString(),
            receiptId = createMovement.receiptId
        )

        // получить все движения после нового движения (не пустая коллекция если вставляем в середину)
        val movementsAfterTargetDate =
            repository.getAllMovementsByAccountIdAfterDate(accountId, targetDate).toMutableList()

        // получить последнее движение перед датой нового
        // определить последний баланс
        val movementBefore = getLastMovementByAccountIdBeforeDate(accountId, targetDate)

        val movementsToSave =
            if (movementsAfterTargetDate.isNotEmpty())
                SortedMovementsCollection(movementsAfterTargetDate)
                    .insertMovementByDate(newMovement)
                    .recalculateDates()
                    .recalculateBalances(movementBefore?.balance ?: BigDecimal.ZERO)
                    .existingMovements
            else calculateSingleBalance(newMovement, movementBefore)

        repository.saveAll(movementsToSave)

        return newMovement
    }

    private fun calculateSingleBalance(
        newMovement: MovementEntity, movementBefore: MovementEntity?
    ): List<MovementEntity> {
        val newBalance = movementBefore?.let {
            it.balance + newMovement.operationAmount * newMovement.type.sign
        } ?: newMovement.operationAmount
        return listOf(newMovement.copy(balance = newBalance)) // TODO я понимаю, что возвращать список удобнее для вызывающего кода, но все же это не то, что должен делать метод
    }

    @Transactional
    fun update(createMovement: CreateMovementDTO): MovementEntity {
        val existingMovement = getOne(createMovement.id)

        val (_, period) = validate(createMovement, existingMovement)

        val amount = createMovement.amount
        var targetDate = LocalDateTime.of(createMovement.date, createMovement.time)
        val accountId = createMovement.accountId


        val updateMovement = existingMovement.copy(
            categoryId = createMovement.categoryId,
            accountId = accountId,
            periodId = period.id,
            date = targetDate,
            operationAmount = amount,
            comment = createMovement.comment,
            balance = amount,  // По-умолчанию равен сумме операции, в дальнейшем пересчитывается
            store = createMovement.store,
            fileIds = createMovement.fileIds.toString()
        )

        val isAccountChanged = existingMovement.accountId != accountId

        if (isAccountChanged)
            return updateAccountChanged(existingMovement, updateMovement)
        else {

            // получить все движения после исходной даты (за исключением редактируемого) (вперед)
            val movementsAfterExistingDate =
                repository.getAllMovementsByAccountIdAfterDate(accountId, existingMovement.date)
                    .filter { it.id != existingMovement.id }
                    .toMutableList()

            // получить все движения после target даты (за исключением редактируемого) (назад)
            val movementsAfterTargetDate = repository.getAllMovementsByAccountIdAfterDate(accountId, targetDate)
                .filter { it.id != existingMovement.id }
                .toMutableList()


            // определить последний баланс
            // получить последнее движение перед датой редактируемого
            // для движения вперед
            val movementBeforeExistingDate =
                getLastMovementByAccountIdBeforeDate(accountId, existingMovement.date)

            // для движения назад
            val movementBeforeTargetDate = getLastMovementByAccountIdBeforeDate(accountId, targetDate)

            // определить движение вперед или назад
            val isForward = existingMovement.date.isBefore(targetDate)

            // определить вставка в середину или в конец (необходимо только для движения назад)
            val intoMiddle = movementsAfterTargetDate.isNotEmpty()

            val movementsToSave =
                if (isForward)
                    ExistingForwardSortedMovementsCollection(movementsAfterExistingDate)
                        .insertMovementByDate(updateMovement)
                        .recalculateDates()
                        .recalculateBalances(lastBalance = movementBeforeExistingDate?.balance ?: BigDecimal.ZERO)
                        .existingMovements
                else // если назад
                    if (intoMiddle)
                        SortedMovementsCollection(movementsAfterTargetDate)
                            .insertMovementByDate(updateMovement)
                            .recalculateDates()
                            .recalculateBalances(lastBalance = movementBeforeTargetDate?.balance ?: BigDecimal.ZERO)
                            .existingMovements
                    else // если в конец
                        calculateSingleBalance(updateMovement, movementBeforeTargetDate)

            repository.saveAll(movementsToSave)

            // TODO переход на другой счет пересчитывает на исходном счету (с начала на исходном счету в конец на целевом)
            return updateMovement
        }
    }
    
    fun update (movementEntity: MovementEntity){
        //  
    }
    fun updateAccountChanged(existingMovement: MovementEntity, updateMovement: MovementEntity): MovementEntity {

        val targetAccountId = updateMovement.accountId
        val targetDate = updateMovement.date

        // получить все движения на исходном счету после исходной даты (за исключением редактируемого)
        val movementsAfterExistingDate =
            repository.getAllMovementsByAccountIdAfterDate(existingMovement.accountId, existingMovement.date)
                .filter { it.id != existingMovement.id }
                .toMutableList()

        // у движений на исходном пересчитать балансы
        val sourceMovementsToUpdate = SortedMovementsCollection(movementsAfterExistingDate)
            .recalculateBalances(existingMovement.balance - existingMovement.operationAmount)
            .existingMovements

        // получить движения на целевом счету после целевой даты
        val movementsAfterTargetDate =
            repository.getAllMovementsByAccountIdAfterDate(targetAccountId, targetDate)
                .toMutableList()

        // получить последнее движение перед датой нового
        // определить последний баланс
        val movementBefore = getLastMovementByAccountIdBeforeDate(targetAccountId, targetDate)

        // пересчитать даты и балансы (как для нового движения)
        val targetMovementsToUpdate =
            if (movementsAfterTargetDate.isNotEmpty())
                SortedMovementsCollection(movementsAfterTargetDate)
                    .insertMovementByDate(updateMovement)
                    .recalculateDates()
                    .recalculateBalances(movementBefore?.balance ?: BigDecimal.ZERO)
                    .existingMovements
            else calculateSingleBalance(updateMovement, movementBefore)

        sourceMovementsToUpdate.addAll(targetMovementsToUpdate)

        repository.saveAll(sourceMovementsToUpdate)
        return updateMovement
    }

    fun getLastMovementByAccountIdBeforeDate(
        accountId: UUID,
        targetDate: LocalDateTime
    ) = repository.getLastMovementByAccountIdBeforeDate(accountId, targetDate)

    fun validate(createMovement: CreateMovementDTO, existingMovement: MovementEntity?) =
        validator.validate(createMovement, existingMovement)

    fun findAll(type: MovementTypeEnum, filter: CategoryGroupFilterDTO): List<CategoryGroupDTO> {
        val (startDate, endDate, periodId) = extractDates(
            filter.startDate, filter.endDate, filter.rangeType, filter.periodId
        )

        /**
         * Детское (50)
         *  Игрушки 20 (50)
         *      Логические 30 <--
         *  Одежда
         */
        var categoriesHierarchical = categoryService
            .findAllRecursively(type)
           
        
        val totalsByCategories: List<TotalByCategory> =
            findSumAmountGroupByCategory(startDate, endDate, periodId, filter.accountId, type)

        val categoryGroupDtoList = categoriesHierarchical.map {
            CategoryGroupDTO(
                categoryId = it.getId(),
                parentCategoryId = it.getParentId(),
                depth = it.getDepth(),
                name = it.getName(),
                own = totalsByCategories.find { c -> c.categoryId == it.getId() }?.amount ?: BigDecimal.ZERO
            )
        }

        categoryGroupDtoList.forEach {
            it.child = findChildAmounts(categoryGroupDtoList, it)
        }

        return categoryGroupDtoList
            .filter { (it.own != BigDecimal.ZERO || it.child != BigDecimal.ZERO) }
            .filter { isNotTransfer(it) }
            .sortedByDescending { it.own + it.child }
    }

    private fun isNotTransfer(it: CategoryGroupDTO): Boolean {
        return it.categoryId != settingsService.senderCategoryId
                && it.categoryId != settingsService.receiverCategoryId
    }

    private fun findChildAmounts(categoryGroupDtoList: List<CategoryGroupDTO>, next: CategoryGroupDTO): BigDecimal {
        var childAmount: BigDecimal = BigDecimal.ZERO

        getAllChilds(categoryGroupDtoList, next.categoryId).forEach {
            childAmount = childAmount + it.own + findChildAmounts(categoryGroupDtoList, it)
        }

        return childAmount
    }

    private fun getAllChilds(categoryGroupDtoList: List<CategoryGroupDTO>, categoryId: String): List<CategoryGroupDTO> {
        return categoryGroupDtoList.filter { it.parentCategoryId == categoryId }
    }

    private fun findSumAmountGroupByCategory(
        startDate: LocalDateTime?, endDate: LocalDateTime?, periodId: UUID?, accountId: UUID?, type: MovementTypeEnum
    ): List<TotalByCategory> {

        val cb = entityManager.criteriaBuilder
        var cq: CriteriaQuery<Array<Any>> = cb.createQuery(Array<Any>::class.java)
        val root: Root<MovementEntity> = cq.from(MovementEntity::class.java)
        val categoryIdPath = root.get<String>(MovementEntity::categoryId.name)
        val datePath = root.get<LocalDateTime>(MovementEntity::date.name)

        cq.where(cb.and(
            startDate?.let {
                if (endDate == null) // эта ветка должна быть пуста
                    cb.equal(datePath, startDate)
                else cb.greaterThanOrEqualTo(datePath, startDate)
            } ?: cb.conjunction(),

            endDate?.let { cb.lessThanOrEqualTo(datePath, endDate) } ?: cb.conjunction(),
            periodId?.let { cb.equal(root.get<UUID>(MovementEntity::periodId.name), periodId) } ?: cb.conjunction(),
            accountId?.let { cb.equal(root.get<UUID>(MovementEntity::accountId.name), accountId) } ?: cb.conjunction(),
            cb.equal(root.get<MovementTypeEnum>(MovementEntity::type.name), type),
        ))

        cq.multiselect(categoryIdPath, cb.sum(root.get<BigDecimal>(MovementEntity::operationAmount.name)))
            .groupBy(categoryIdPath)

        return entityManager.createQuery(cq).resultList.map {
            TotalByCategory(
                it[0] as String,
                (it[1] as BigDecimal).abs(),
                null
            )
        }
    }

    fun movements(
        type: MovementTypeEnum,
        filter: CategoryGroupFilterDTO,
        categoryId: String
    ): Map<LocalDate, List<MovementTableDTO>> {
        val (startDate, endDate, periodId) = extractDates(
            filter.startDate, filter.endDate, filter.rangeType, filter.periodId
        )

        val movements: List<MovementEntity> = repository.findByCategoryAndFilter(
            type,
            categoryId,
            startDate,
            endDate,
            periodId,
            filter.accountId,
            startDate == null,
            endDate == null
        )

        return movementsWithTransferInfo(movements)
    }


    fun history(filter: CategoryGroupFilterDTO): Map<LocalDate, List<MovementTableDTO>> {
        val (startDate, endDate, periodId) = extractDates(
            filter.startDate, filter.endDate, filter.rangeType, filter.periodId
        )

        val movements = repository.findAllByFilter(
            startDate,
            endDate,
            periodId,
            filter.accountId,
            startDate == null,
            endDate == null,
        )

        return movementsWithTransferInfo(movements)
    }

    private fun movementsWithTransferInfo(movements: List<MovementEntity>): Map<LocalDate, List<MovementTableDTO>> {
        // id движений, которые являются переводами или коммисией за переводы
        val idsThatTransfers = movements
            .filter { isMovementTransferFee(it) || it.isTransfer }
            .map { it.id }

        // переводы этих движений
        val transfers: List<TransferEntity> = transferRepository.findByMovementIds(idsThatTransfers)

        val c = movements
            .map { movement ->
                //  найти этот перевод для данного движения или комиссии
                val transferByMovement = transfers.find {
                    it.sourceMovement.id == movement.id
                            || it.targetMovement.id == movement.id
                            || it.fee?.id == movement.id
                }

                movement.toTableDto(transferByMovement?.id, isMovementTransferFee(movement))
            }

        val v = c.groupBy { it.date }

        return v
    }

    
    fun getLastMovementsByAccountIds(accountIds: List<UUID>) = repository.getLastMovementsByAccountIds(accountIds)

    fun findAllByAccountId(id: UUID) = repository.findAllByAccountId(id)

    fun dailyMovements(type: MovementTypeEnum): BigDecimal {

        return repository.findAllByTypeAndDateBetweenAndCategoryIdNotIn(
            type, LocalDate.now().atStartOfDay(), LocalDate.now().toLocalDateTimeEnd(), transferCategoryIds()
        ).fold(BigDecimal(0)) { prev, outcome -> prev.plus(outcome.operationAmount) }
    }

    fun periodMovements(type: MovementTypeEnum): BigDecimal {
        return periodRepository.findCurrentPeriod()?.let {
            repository.findAllByPeriodAndTypeAndCategoryIdNotIn(it, type, transferCategoryIds())
                .fold(BigDecimal(0)) { prev, outcome -> prev.plus(outcome.operationAmount) }
        } ?: BigDecimal(0)
    }

    fun getOne(id: UUID?): MovementEntity {
        return repository.findByIdOrNull(id)
            ?: throw IllegalArgumentException("Movement with id $id not found")
    }

    @Transactional
    fun delete(id: UUID) {
        val existingMovement = repository.findByIdOrNull(id) ?: return

        // после удаления пересчитать все балансы после даты удаляемого на том же счету
        val movementsAfterDeleted =
            repository.getAllMovementsByAccountIdAfterDate(existingMovement.accountId, existingMovement.date)
                .filter { it.id != existingMovement.id }
                .toMutableList()

        val movementsToUpdate = SortedMovementsCollection(movementsAfterDeleted)
            .recalculateBalances(existingMovement.balance - (existingMovement.operationAmount) * existingMovement.type.sign)
            .existingMovements

        repository.deleteById(id)

        repository.saveAll(movementsToUpdate)
    }

    fun transferCategoryIds(): List<String> {
        val senderCategoryId = settingsService.findByKey(SettingsKey.TRANSFER_SENDER_CATEGORY_ID)
        val receiverCategoryId = settingsService.findByKey(SettingsKey.TRANSFER_RECEIVER_CATEGORY_ID)
        return listOf(senderCategoryId, receiverCategoryId)
    }

    private fun isMovementTransferFee(movement: MovementEntity) =
        movement.categoryId == settingsService.findByKey(SettingsKey.FEE_CATEGORY_ID)
    
    fun lastDate() = repository.lastDate()

    fun getLastActionByAccount(accountId: UUID, id: UUID? = null) =
        repository.getLastMovementByAccountId(accountId, id.toString())

}