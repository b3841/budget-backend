package com.djadiv.budgetbackendv3.movements


// Для существующих
//  - Движение вперед
//  - - В середину
// Получить все движения после исходной даты
// Вставить редактируемое движение в середину; Получить коллекцию
// Пересчитать даты после того места, где вставили редактируемое движение
// пересчитать балансы с начала
// - - В конец
// Все то же самое, за исключением того, что не нужно в коллекцию добавлять движения после target date
// Можно использовать этот класс, все будет работать
class ExistingForwardSortedMovementsCollection(movementsAfterExistingDate: MutableList<MovementEntity>) :
    SortedMovementsCollection(movementsAfterExistingDate) {

    lateinit var newMovement: MovementEntity;

    //  - Движение вперед
    //  - - В середину
    override fun insertMovementByDate(newMovement: MovementEntity): ExistingForwardSortedMovementsCollection {
        val newMovementsCollection = mutableListOf<MovementEntity>()

        // добавить движения перед target датой
        newMovementsCollection.addAll(existingMovements.filter { it.date < newMovement.date })
        // добавить редактируемое движение
        newMovementsCollection.add(newMovement)
        this.newMovement = newMovement
        // добавить движения после target даты (они существуют, так как вставка в середину)
        newMovementsCollection.addAll(existingMovements.filter { it.date >= newMovement.date })
        existingMovements = newMovementsCollection

        return this
    }

    // Пересчитать даты после того места, где вставили редактируемое движение
    override fun recalculateDates(): SortedMovementsCollection {
        recalculateDatesAfter(newMovement)
        return this
    }
}