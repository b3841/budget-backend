package com.djadiv.budgetbackendv3.movements

import com.djadiv.budgetbackendv3.movements.dto.CreateMovementDTO
import com.djadiv.budgetbackendv3.categories.CategoryService
import com.djadiv.budgetbackendv3.periods.PeriodsService
import org.springframework.stereotype.Component
import com.djadiv.budgetbackendv3.errors.excepions.IllegalArgumentException
import com.djadiv.budgetbackendv3.accounts.AccountService2
import com.djadiv.budgetbackendv3.accounts.AccountEntity
import com.djadiv.budgetbackendv3.periods.PeriodEntity
import com.djadiv.budgetbackendv3.utils.formatWithYear
import com.djadiv.budgetbackendv3.utils.toLocalDateTimeStart


@Component
class MovementCreateValidator(
    private val categoryService: CategoryService,
    private val periodsService: PeriodsService,
    private val accountService2: AccountService2
) {

    fun validate(createMovement: CreateMovementDTO, existingMovement: MovementEntity?): Pair<AccountEntity, PeriodEntity> {
        val category = categoryService.findById(createMovement.categoryId)
        val account = accountService2.findById(createMovement.accountId)

        val date = createMovement.date.toLocalDateTimeStart()
        val period = periodsService.periodByDate(date)
            ?: throw IllegalArgumentException(
                "There is no period with date $date",
                "Указанная дата ${date.formatWithYear()} не входит ни в один период"
            )

        if (category.type != createMovement.type)
            throw IllegalArgumentException(
                "Type of category [id: ${category.id}], [name: ${category.name}], " +
                        "[type: ${category.type}] does not match income parameter [type: ${createMovement.type}]"
            )

        existingMovement?.let {
            if (it.type != createMovement.type)
                throw IllegalArgumentException(
                    "Type of existing movement [id: ${it.id}], [type: ${it.type}]" +
                            " does not match income parameter [type: ${createMovement.type}]"
                )
        }

        return account to period
    }

}