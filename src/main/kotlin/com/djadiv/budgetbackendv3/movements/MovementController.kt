package com.djadiv.budgetbackendv3.movements

import com.djadiv.budgetbackendv3.movements.dto.CategoryGroupDTO
import com.djadiv.budgetbackendv3.movements.dto.CategoryGroupFilterDTO
import com.djadiv.budgetbackendv3.movements.dto.CreateMovementDTO
import com.djadiv.budgetbackendv3.enums.MovementTypeEnum
import com.djadiv.budgetbackendv3.enums.RangeTypeEnum
import com.djadiv.budgetbackendv3.movements.dto.MovementTableDTO
import org.springframework.web.bind.annotation.*
import java.math.BigDecimal
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

@RestController
@RequestMapping("/api/private/v1/movements")
class MovementController(
    private val service: MovementService,
) {

    @PostMapping
    fun table(
        @RequestParam type: MovementTypeEnum,
        @RequestBody filter: CategoryGroupFilterDTO
    ): List<CategoryGroupDTO> {
        println(filter)
        return service.findAll(type, filter)
    }

    @PostMapping("/movement")
    fun movements(
        @RequestParam type: MovementTypeEnum,
        @RequestParam categoryId: String,
        @RequestBody filter: CategoryGroupFilterDTO
    ): Map<LocalDate, List<MovementTableDTO>> {
        println(filter)
        return service.movements(type, filter, categoryId)
    }

    @GetMapping("/total")
    fun getOutcomesByRange(
        @RequestParam rangeType: RangeTypeEnum,
        @RequestParam type: MovementTypeEnum,
    ): BigDecimal {
        return if (rangeType == RangeTypeEnum.DAY)
            service.dailyMovements(type)
        else if (rangeType == RangeTypeEnum.PERIOD)
            service.periodMovements(type)
        else BigDecimal(0)
    }

    @PostMapping("/save")
    fun save(@RequestBody createMovement: CreateMovementDTO) {
        if (createMovement.id == null)
            service.create(createMovement)
        else
            service.update(createMovement)
    }

    @GetMapping("/{id}")
    fun getOne(@PathVariable id: UUID): CreateMovementDTO {
        return service.getOne(id).toCreateDto()
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: UUID) {
        service.delete(id)
    }


    @PostMapping("/history")
    fun history(
        @RequestBody filter: CategoryGroupFilterDTO
    ): Map<LocalDate, List<MovementTableDTO>> {
        return service.history(filter)
    }

    // following for test API from Postman
    @GetMapping("/lastDate")
    fun lastDay() = service.lastDate().plusSeconds(1)

    @GetMapping("/lastBalance")
    fun getLastBalanceOnAccountBeforeDate(
        @RequestParam accountId: UUID,
        @RequestParam date: LocalDateTime
    ): BigDecimal? {
        return service.getLastMovementByAccountIdBeforeDate(accountId, date)?.balance
    }
}