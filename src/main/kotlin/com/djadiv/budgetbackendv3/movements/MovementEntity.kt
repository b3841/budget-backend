package com.djadiv.budgetbackendv3.movements

import com.djadiv.budgetbackendv3.accounts.AccountEntity
import com.djadiv.budgetbackendv3.categories.CategoryEntity
import com.djadiv.budgetbackendv3.periods.PeriodEntity
import com.djadiv.budgetbackendv3.enums.MovementTypeEnum
import com.djadiv.budgetbackendv3.movements.dto.CreateMovementDTO
import com.djadiv.budgetbackendv3.movements.dto.MovementTableDTO
import com.djadiv.budgetbackendv3.receipts.ReceiptEntity
import java.math.BigDecimal
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.util.*
import jakarta.persistence.*

@Entity
@Table(name = "movements")
//@TypeDef(
//    name = "postgres-uuid",
//    defaultForType = UUID::class,
//    typeClass = PostgresUUIDType::class
//)
data class MovementEntity(

    @Id
    // вся эта херня не работает
//    @Type(type="pg-uuid")
//    @Type(type="org.hibernate.type.PostgresUUIDType")
//    @org.hibernate.annotations.Type(type="uuid-char")
    val id: UUID = UUID.randomUUID(),

    @Column(name = "category_id")
    val categoryId: String,

    @ManyToOne
    @JoinColumn(name = "category_id", insertable = false, updatable = false)
    val categoryEntity: CategoryEntity? = null,

    @Column(name = "account_id")
    val accountId: UUID,

    @ManyToOne
    @JoinColumn(name = "account_id", insertable = false, updatable = false)
    val account: AccountEntity? = null,

    @Column(name = "period_id")
    val periodId: UUID,

    @ManyToOne
    @JoinColumn(name = "period_id", insertable = false, updatable = false)
    val period: PeriodEntity? = null,

    @Column(name = "m_date")
    val date: LocalDateTime,

    @Column(name = "operation_amount")
    val operationAmount: BigDecimal = BigDecimal.ZERO, // store without sign

    val comment: String? = null,

    var balance: BigDecimal = BigDecimal.ZERO,

    @Enumerated(EnumType.STRING)
    val type: MovementTypeEnum = MovementTypeEnum.OUTCOME,

    val isTransfer: Boolean = false,

    val store: String? = null,

    val fileIds: String? = null,
    
    @Column(name = "receipt_id")
    val receiptId: UUID? = null,
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "receipt_id", insertable = false, updatable = false)
    val receipt: ReceiptEntity? = null

) {
    fun toTableDto(transferByMovementId: UUID?, isTransferFee: Boolean) = MovementTableDTO(
        id = this.id,
        categoryId = categoryId,
        categoryName = categoryEntity!!.name,
        accountName = account!!.name,
        accountId = accountId,
        date = LocalDate.from(date),
        time = LocalTime.from(date),
        amount = operationAmount.abs(),
        balance = balance,
        comment = comment,
        type = type,
        isTransfer = isTransfer,
        isTransferFee = isTransferFee,
        transferId = transferByMovementId,
        store = store,
        receiptId = receiptId
    )

    fun toCreateDto() = CreateMovementDTO(
        id = id,
        categoryId = categoryId,
        accountId = accountId,
        date = LocalDate.from(date),
        time = LocalTime.from(date),
        amount = operationAmount.abs(),
        balance = balance,
        comment = comment,
        type = type,
        store = store,
        receiptId = receiptId
//        fileIds = files
    )
}