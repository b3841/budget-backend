package com.djadiv.budgetbackendv3.repositories.test

data class SearchCriteria (
     val key: String,
     val operation: String,
     val value: String
    )