package com.djadiv.budgetbackendv3.repositories.test

import org.springframework.stereotype.Repository
import jakarta.persistence.EntityManager
import jakarta.persistence.PersistenceContext
import jakarta.persistence.criteria.CriteriaQuery
import jakarta.persistence.criteria.Path
import jakarta.persistence.criteria.Predicate
import jakarta.persistence.criteria.Root


@Repository
class TestDAo(
    @PersistenceContext
    private val entityManager: EntityManager
) {

    fun searchUser(params: List<SearchCriteria?>): List<User>? {
        val criteriaBuilder = entityManager.criteriaBuilder

        val query: CriteriaQuery<User> = criteriaBuilder.createQuery(User::class.java)

        val root: Root<User> = query.from(User::class.java)
        var predicate: Predicate = criteriaBuilder.conjunction()

        val searchConsumer = UserSearchQueryCriteriaConsumer(predicate, criteriaBuilder, root)

        params
            .forEach {
                searchConsumer
            }

        predicate = searchConsumer.predicate

        val where = query.where(predicate)

        val pathShippingAddress: Path<String> = root.get("shippingAdress")
        query.select(root.get("shippingAdress"))

        return entityManager.createQuery(query).getResultList()
    }
}