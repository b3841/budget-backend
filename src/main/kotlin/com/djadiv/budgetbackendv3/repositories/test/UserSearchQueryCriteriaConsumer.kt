package com.djadiv.budgetbackendv3.repositories.test

import java.util.function.Consumer
import jakarta.persistence.criteria.CriteriaBuilder
import jakarta.persistence.criteria.Predicate
import jakarta.persistence.criteria.Root


class UserSearchQueryCriteriaConsumer(
    var predicate: Predicate,
    private val criteriaBuilder: CriteriaBuilder,
    private val r: Root<*>
) : Consumer<SearchCriteria> {


    val searchPredicates: MutableList<Predicate> = mutableListOf()

    override fun accept(param: SearchCriteria) {

        if (param.operation.equals(">", true)) {

            val greaterThanOrEqualTo: Predicate = criteriaBuilder.greaterThanOrEqualTo(r.get(param.key), param.value)

            searchPredicates.add(greaterThanOrEqualTo)

//            predicate = criteriaBuilder.and(predicate, criteriaBuilder.greaterThanOrEqualTo(r.get(param.key), param.value.toString()))

        } else if (param.operation.equals("<", true)) {

            val lessThanOrEqualTo: Predicate = criteriaBuilder.lessThanOrEqualTo(r.get(param.key), param.value)

            searchPredicates.add(lessThanOrEqualTo)

//            predicate = criteriaBuilder.and(predicate, criteriaBuilder.lessThanOrEqualTo(r.get(param.key), param.value.toString()))

        } else if (param.operation.equals(":", true)) {

            predicate =
                if (r.get<String>(param.key)?.getJavaType() === String::class.java)
                    criteriaBuilder.and(predicate, criteriaBuilder.like(r.get(param.key), "%" + param.value.toString() + "%"))
                else
                    criteriaBuilder.and(predicate, criteriaBuilder.equal(r.get<String>(param.key), param.value))
        }
    }
}
