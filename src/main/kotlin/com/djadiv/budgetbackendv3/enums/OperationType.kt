package com.djadiv.budgetbackendv3.enums

enum class OperationType(var stringValue: String) {
    MOVEMENT("MOVEMENT"),
    TRANSFER("TRANSFER")
}