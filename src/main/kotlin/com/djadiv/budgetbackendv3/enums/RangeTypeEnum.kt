package com.djadiv.budgetbackendv3.enums

enum class RangeTypeEnum {
    DAY,
    WEEK,
    MONTH,
    PERIOD,
    DIAPASON,
    ALL

}