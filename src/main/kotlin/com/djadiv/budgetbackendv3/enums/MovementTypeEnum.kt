package com.djadiv.budgetbackendv3.enums

import java.math.BigDecimal

enum class MovementTypeEnum(var type: String, var sign: BigDecimal) {
    OUTCOME("OUTCOME", BigDecimal(-1)),
    INCOME("INCOME", BigDecimal(1))
}