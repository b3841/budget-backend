package com.djadiv.budgetbackendv3.enums

enum class CurrencyEnum {
    KZT,
    USD,
    EUR,
    UZS,
    RUB
}