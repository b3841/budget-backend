package com.djadiv.budgetbackendv3.enums

enum class TableNames(var tableName: String) {
    OUTCOME_CATEGORIES("outcome_categories"),
    INCOME_CATEGORIES("income_categories")
}