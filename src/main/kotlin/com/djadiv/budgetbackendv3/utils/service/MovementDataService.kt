package com.djadiv.budgetbackendv3.utils.service

import com.djadiv.budgetbackendv3.accounts.AccountEntity
import com.djadiv.budgetbackendv3.accounts.AccountRepository
import com.djadiv.budgetbackendv3.accounts.AccountService
import com.djadiv.budgetbackendv3.enums.MovementTypeEnum
import com.djadiv.budgetbackendv3.errors.excepions.IllegalArgumentException
import com.djadiv.budgetbackendv3.movements.MovementEntity
import com.djadiv.budgetbackendv3.movements.MovementRepository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.math.BigDecimal
import java.time.LocalDateTime
import java.util.*

@RestController
@RequestMapping("/api/private/v1/service/movements")
class MovementDataService(
    private val movementRepository: MovementRepository,
    private val accountRepository: AccountRepository,
    private val accountService: AccountService
) {

    @GetMapping("recalculateBalances")
    fun recalculate(
        @RequestParam accountId: UUID? = null, @RequestParam lastBalance: BigDecimal? = null
    ) {
        if (accountId == null && lastBalance == null) recalculateAllAccountsAuto()
        else recalculateByAccount(accountId, lastBalance)
    }
    
    private fun recalculateByAccount(accountId: UUID?, lastBalance: BigDecimal?) {
        if (accountId == null) throw IllegalArgumentException("accountId cannot be null")

        val account = accountService.findById(accountId)
        updateOnAccount(account, lastBalance)
    }

    private fun recalculateAllAccountsAuto() {
        accountRepository.findAll().forEach { account ->
            updateOnAccount(account, null)
        }
    }

    private fun updateOnAccount(account: AccountEntity, lastBalance: BigDecimal?) {
        var first = lastBalance == null // if lastBalance present use it
        var prevBalance = lastBalance ?: BigDecimal.ZERO
        var prevAmount = BigDecimal.ZERO
        var prevType = MovementTypeEnum.INCOME

        val updatedAccountMovements = movementRepository.findAllByAccountId(account.id).map { movement ->
            // Take last balance from last mov
            if (first) {
                prevBalance = movement.balance
                first = false
            } else {
                movement.balance = if (prevType == MovementTypeEnum.OUTCOME) prevBalance + prevAmount
                else prevBalance - prevAmount
            }

            prevBalance = movement.balance
            prevAmount = movement.operationAmount
            prevType = movement.type
            movement
        }
        movementRepository.saveAll(updatedAccountMovements)

        print("Updated ${updatedAccountMovements.size} movements for account ${account.name}")
    }
    
    @GetMapping("updateDuplicatedTimes")
    fun updateTimeStamps() {
        val resultList = arrayListOf<MovementEntity>();
        val list = movementRepository.findAllTimestampDuplicates()
        val byAccountMap: Map<UUID, List<MovementEntity>> = list.groupBy { it.accountId }

        byAccountMap.forEach { byAccount ->
            val byDateMap: Map<LocalDateTime, List<MovementEntity>> = byAccount.value.groupBy { it.date }

            byDateMap.forEach { byDate ->
                byDate.value.forEachIndexed { i, mov ->
                    if (i != 0) resultList.add(mov.copy(date = mov.date.plusSeconds(i.toLong())))
                }
            }
        }
        println("------" + resultList.size)
        movementRepository.saveAll(resultList)
    }
}