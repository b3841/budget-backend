package com.djadiv.budgetbackendv3.utils

fun String.subs(charsCount: Int): String {
    return if (this.length > charsCount)
        this.substring(0, charsCount - 1) + ".."
    else this
}