package com.djadiv.budgetbackendv3.utils

import java.math.BigDecimal
import java.text.DecimalFormat

fun BigDecimal.format(): String{
    var currencyFormat = DecimalFormat("#,###")
    return currencyFormat.format(this)
}