package com.djadiv.budgetbackendv3.utils

import com.djadiv.budgetbackendv3.enums.RangeTypeEnum
import org.junit.jupiter.api.Assertions
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.util.*

public enum class Formatter(val f: DateTimeFormatter) {
    DD_MMM(DateTimeFormatter.ofPattern("dd MMM")),
    DD_MMM_HH_MIN(DateTimeFormatter.ofPattern("dd MMM hh:mm")),
    DD_MM_YYYY(DateTimeFormatter.ofPattern("dd MM yyyy")),
    DD_MM_YYYY_HH_MIN_SS(DateTimeFormatter.ofPattern("dd MM yyyy HH:mm:ss"))

}

fun LocalDateTime.format(formatter: Formatter): String = this.format(formatter.f)
fun LocalDateTime.formatWithYear(): String = this.format(Formatter.DD_MM_YYYY)

fun LocalDateTime.toWord(f: Formatter): String {
    return when (ChronoUnit.DAYS.between(LocalDate.now(), this)) {
        0L -> "Сегодня"
        1L -> "Вчера"
        2L -> "Позавчера"
        else -> this.format(f)
    }
}

fun LocalDate.toInstant(): Instant =
    Instant.ofEpochSecond(
        this.toEpochSecond(
            LocalTime.of(0, 0, 0, 0),
            ZoneOffset.ofHours(0)
        )
    )


fun LocalDate.toLocalDateTimeStart(): LocalDateTime =
    this.atStartOfDay()

fun LocalDate.toLocalDateTimeEnd(): LocalDateTime =
    this.atTime(23, 59, 59)

fun LocalDate.toLocalDateTimeNow(): LocalDateTime =
    this.atTime(LocalTime.now())

fun extractDates(
    startDate: LocalDate?, endDate: LocalDate?,
    rangeType: RangeTypeEnum, periodId: UUID?
)
        : Triple<LocalDateTime?, LocalDateTime?, UUID?> {
    var startDateTime = startDate?.toLocalDateTimeStart()
    var endDateTime = endDate?.toLocalDateTimeEnd() ?: startDate?.toLocalDateTimeEnd()

    return when (rangeType) {
        RangeTypeEnum.ALL -> Triple(null, null, null)
        RangeTypeEnum.DAY -> {
            Assertions.assertNotNull(startDateTime, "startDate cannot be null")
            Triple(startDateTime, endDateTime, null)
        }
        RangeTypeEnum.WEEK, RangeTypeEnum.MONTH, RangeTypeEnum.DIAPASON -> {
            Assertions.assertNotNull(startDateTime, "startDate cannot be null")
            Assertions.assertNotNull(endDateTime, "endDate cannot be null")
            Triple(startDateTime, endDateTime, null)
        }
        RangeTypeEnum.PERIOD -> {
            Assertions.assertNotNull(periodId, "periodId cannot be null")
            Triple(null, null, periodId)
        }
    }
}