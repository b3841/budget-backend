package com.djadiv.budgetbackendv3.settings

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface SettingsRepository : JpaRepository<SettingEntity, Int>{
}