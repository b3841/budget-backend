package com.djadiv.budgetbackendv3.settings

enum class SettingsKey(val settingKey: String) {

    FEE_CATEGORY_ID("FEE_CATEGORY_ID"),
    TRANSFER_SENDER_CATEGORY_ID("TRANSFER_SENDER_CATEGORY_ID"),
    TRANSFER_RECEIVER_CATEGORY_ID("TRANSFER_RECEIVER_CATEGORY_ID"),
}