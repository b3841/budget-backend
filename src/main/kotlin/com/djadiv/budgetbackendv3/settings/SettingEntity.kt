package com.djadiv.budgetbackendv3.settings

import jakarta.persistence.*

@Table(name = "settings")
@Entity
data class SettingEntity(

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Int = 0,

    @Column
    val key: String,

    @Column
    val value: String
)