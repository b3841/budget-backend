package com.djadiv.budgetbackendv3.settings

import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Service

@Service
class SettingsService(
    private val repository: SettingsRepository
) {
    lateinit var senderCategoryId: String
    lateinit var receiverCategoryId: String

    private val map = mutableMapOf<String, String>()

    @EventListener(ContextRefreshedEvent::class)
    fun loadAll() {
        map.putAll(repository.findAll().associate { it.key to it.value })
        senderCategoryId = findByKey(SettingsKey.TRANSFER_SENDER_CATEGORY_ID)
        receiverCategoryId = findByKey(SettingsKey.TRANSFER_RECEIVER_CATEGORY_ID)
    }

    fun findByKey(key: SettingsKey): String {
        return this.map[key.settingKey]!!
    }

}