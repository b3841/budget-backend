package com.djadiv.budgetbackendv3.cache.persons

import com.redis.om.spring.annotations.Document
import com.redis.om.spring.annotations.Indexed
import com.redis.om.spring.annotations.Searchable
import org.springframework.data.annotation.Id
import org.springframework.data.geo.Point
import org.springframework.lang.NonNull

@Document
data class Person(
    
    @Id
    @Indexed
    var id: String? = null,

    @Indexed
    @NonNull
    var firstName: String,

    @Indexed
    @NonNull
    val lastName: String,

//Indexed for numeric matches
    @Indexed
    @NonNull
    val age: Int,

//Indexed for Full Text matches
    @Searchable
    @NonNull
    val personalStatement: String,

//Indexed for Geo Filtering
    @Indexed
    @NonNull
    val homeLoc: Point,

// Nest indexed object
    @Indexed
    @NonNull
    val address: Address,

    @Indexed
    @NonNull
    val skills: Set<String>,
)

