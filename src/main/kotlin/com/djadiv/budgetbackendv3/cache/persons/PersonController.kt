package com.djadiv.budgetbackendv3.cache.persons

import org.springframework.data.geo.Distance
import org.springframework.data.geo.Metrics
import org.springframework.data.geo.Point
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/api/private/v1/people")
class PersonController(
//    private val repository: PersonRepository,
    private val service: PersonService
) {

    @GetMapping("/all")
    fun all(): Iterable<Person> {
        return service.findAll()
    }

    @GetMapping("/{id}")
    fun byId(@PathVariable id: String): Optional<Person> {
        return service.findById(id)
    }

    @GetMapping("/age_between")
    fun byAgeBetween(
        @RequestParam("min") min: Int,
        @RequestParam("max") max: Int
    ): Iterable<Person> {
        return service.findByAgeBetween(min, max)
    }

//    @GetMapping("/name")
//    fun byFirstNameAndLastName(
//        @RequestParam("first") firstName: String,
//        @RequestParam("last") lastName: String
//    ): Iterable<Person?> {
//        return service.findByFirstNameAndLastName(firstName, lastName)
//    }
//
//    @GetMapping("/homeloc")
//    fun byHomeLoc(
//        @RequestParam("lat") lat: Double,
//        @RequestParam("lon") lon: Double,
//        @RequestParam("distance") distance: Double
//    ): Iterable<Person> {
//        return service.findByHomeLocNear(
//            Point(lon, lat),
//            Distance(distance, Metrics.MILES)
//        )
//    }
//
//    @GetMapping("/city")
//    fun byCity(@RequestParam("city") city: String): Iterable<Person> {
//        return service.findByAddress_City(city)
//    }
//
//    @GetMapping("/skills")
//    fun byAnySkills(@RequestParam("skills") skills: Set<String>): Iterable<Person> {
//        return service.findBySkills(skills)
//    }


}