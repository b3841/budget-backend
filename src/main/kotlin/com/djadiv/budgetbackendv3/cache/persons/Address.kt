package com.djadiv.budgetbackendv3.cache.persons

import com.redis.om.spring.annotations.Indexed
import com.redis.om.spring.annotations.Searchable
import org.springframework.lang.NonNull

data class Address(
    @NonNull
    @Indexed
    var houseNumber: String,

    @NonNull
    @Searchable(nostem = true)
    val street: String,

    @NonNull
    @Indexed
    val city: String,

    @NonNull
    @Indexed
    val state: String,

    @NonNull
    @Indexed
    val postalCode: String,

    @NonNull
    @Indexed
    val country: String,
)