package com.djadiv.budgetbackendv3.cache.persons

import com.redis.om.spring.repository.RedisDocumentRepository
import org.springframework.data.geo.Distance
import org.springframework.data.geo.Point
import org.springframework.stereotype.Repository

@Repository
interface PersonRepository: RedisDocumentRepository<Person, String> {
    
    // Find people by age range
    fun findByAgeBetween(minAge: Int, maxAge: Int): Iterable<Person>

    // Find people by their first and last name
    fun findByFirstNameAndLastName(firstName: String, lastName: String): Iterable<Person>

    // Draws a circular geofilter around a spot and returns all people in that
    // radius
    fun findByHomeLocNear(point: Point, distance: Distance): Iterable<Person>

    // Performs full-text search on a person’s personal Statement
    fun searchByPersonalStatement(text: String): Iterable<Person>

    // Performing a tag search on city
    fun findByAddress_City(city: String): Iterable<Person>

    // Search Persons that have one of multiple skills (OR condition)
    fun findBySkills(skills: Set<String>): Iterable<Person>

}