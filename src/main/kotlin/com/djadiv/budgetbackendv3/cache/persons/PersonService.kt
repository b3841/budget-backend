package com.djadiv.budgetbackendv3.cache.persons

import com.redis.om.spring.search.stream.EntityStream
import org.springframework.stereotype.Service
import redis.clients.jedis.search.aggr.SortedField
import java.util.*
import java.util.stream.Collectors

@Service
class PersonService(
    private val entityStream: EntityStream,
    private val repository: PersonRepository
) {

    // Find all people
    fun findAll(): Iterable<Person> {
        return entityStream
            .of(Person::class.java)
            .limit(10000)
            .collect(Collectors.toList())
    }

    fun findById(id: String): Optional<Person> {
        return repository.findById(id)
    }

    fun findByAgeBetween(min: Int, max: Int): Iterable<Person> {
        return repository.findByAgeBetween(min, max)
    }

    // Find people by age range
//    fun findByAgeBetween(minAge: Integer, maxAge: Integer): List<Person> {
//        return entityStream //
//            .of(Person::class.java) //
//            .filter(`Person$`.AGE.between(minAge, maxAge)) //
//            .sorted(`Person$`.AGE, SortedField.SortOrder.ASC) //
//            .collect(Collectors.toList())
//    }

    // Find people by their first and last name
//    public Iterable<Person> findByFirstNameAndLastName(String firstName, String lastName) {
//        return entityStream
//                .of(Person.class)
//                .filter(((NumericFieldOperationInterceptor<Person, String>) Person$.FIRST_NAME).eq(firstName))
//                .filter(((NumericFieldOperationInterceptor<Person, String>) Person$.FIRST_NAME).eq(lastName))
//                .collect(Collectors.toList());
//    }


}