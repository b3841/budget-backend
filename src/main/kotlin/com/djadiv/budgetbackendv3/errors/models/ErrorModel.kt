package com.djadiv.budgetbackendv3.errors.models

import java.util.UUID
import org.springframework.http.HttpStatus
import com.djadiv.budgetbackendv3.errors.Error

data class ErrorModel(

    //Уникальный идентификатор ошибки
    val id: UUID,

    // Код  статуса HTTP ошибки
    val status: HttpStatus,

    // Код  ошибки
    val code: Error,

    // Текст ошибки
    val error: String? = null,

    // Данные ошибки
    val data: String? = null,

    // Отладочные данные
    val detail: String? = null,

    // Сообщение для пользователя
    val message: String? = null
)
