package com.djadiv.budgetbackendv3.errors.excepions

class FileUploadException : CommonException{
    constructor(message: String, ex: Exception) : super(IllegalArgumentException.ERROR, message, ex)
}