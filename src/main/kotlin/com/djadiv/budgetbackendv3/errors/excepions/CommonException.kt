package com.djadiv.budgetbackendv3.errors.excepions

import java.util.UUID
import kotlin.reflect.KProperty1
import com.djadiv.budgetbackendv3.errors.Error


open class CommonException : RuntimeException {
    val id = UUID.randomUUID()
    val error: Error
    val data: String?
    val userMessage: String?

    constructor (
        error: Error,
        message: String,
        data: String? = null,
        userMessage: String? = null
    ) : super(message) {
        this.error = error
        this.data = data
        this.userMessage = userMessage
    }

    constructor (error: Error, message: String, userMessage: String? = null) : super(message) {
        this.error = error
        this.data = null
        this.userMessage = userMessage
    }

    constructor (
        error: Error,
        message: String,
        ex: Exception,
        data: String? = null,
        userMessage: String? = null
    ) : super(message, ex) {
        this.error = error
        this.data = data
        this.userMessage = userMessage
    }

    constructor (error: Error, message: String, ex: Exception, userMessage: String? = null) : super(message, ex) {
        this.error = error
        this.data = null
        this.userMessage = userMessage
    }
}


data class UserMessage(
    val mess: String
)

class IllegalArgumentException : CommonException {
    companion object {
        val ERROR = Error.ILLEGAL_ARGUMENT
    }

    constructor(message: String, userMessage: String? = null) : super(ERROR, message, userMessage)
    constructor(message: String, ex: Exception) : super(ERROR, message, ex)
}

class ResourceNotFoundException : CommonException {
    companion object {
        val ERROR = Error.NOT_FOUND
    }

    constructor(message: String, userMessage: String? = null) : super(ERROR, message, userMessage)
    constructor(message: String, ex: Exception) : super(ERROR, message, ex)
}
