package com.djadiv.budgetbackendv3.errors

import org.springframework.http.HttpStatus

enum class Error(
    val httpStatus: HttpStatus,
    val text: String
) {
    ILLEGAL_ARGUMENT(HttpStatus.BAD_REQUEST, "Illegal argument"),
    NOT_FOUND(HttpStatus.NOT_FOUND, "Not found"),


    BAD_CREDENTIALS(HttpStatus.BAD_REQUEST, "Bad credentials"),
    INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error"),
    UNKNOWN_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "Unknown error"),
    NOTIFICATION_SEND_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "Error occured while sending notification"),
    INVALID_NOTIFICATION_CONFIGURATION(HttpStatus.INTERNAL_SERVER_ERROR, "Invalid notification configuration"),
    NO_NOTIFICATION_TRANSPORT_AVAILABLE(HttpStatus.INTERNAL_SERVER_ERROR, "No notification transport provider"),
    SEND_SMS_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "Error occured while sending sms"),
    IIN_INVALID(HttpStatus.BAD_REQUEST, "Invalid iin"),
    BAD_REQUEST(HttpStatus.BAD_REQUEST, "Required request body is missing"),
    RESOURCE_NOT_FOUND(HttpStatus.NOT_FOUND, "Resource not found"),
    CODE_INVALID(HttpStatus.BAD_REQUEST, "Otp code is invalid"),
    AUTHENTICATION_ERROR(HttpStatus.UNAUTHORIZED, "Authentication error"),
    PDF_GENERATE_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "Pdf generate error"),
    ACCESS_DENIED_ERROR(HttpStatus.FORBIDDEN, "Access denied"),
    E_SIGN_ERROR(HttpStatus.BAD_REQUEST, "ESign validation error"),
    RESET_PASSWORD_REQUIRED(HttpStatus.FORBIDDEN, "Reset password required"),
    FILE_UPLOAD_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "Не удалось загрузить файл"),
}