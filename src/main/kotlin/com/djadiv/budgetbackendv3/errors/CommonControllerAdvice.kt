package com.djadiv.budgetbackendv3.errors

import com.djadiv.budgetbackendv3.errors.excepions.CommonException
import com.djadiv.budgetbackendv3.errors.excepions.FileUploadException
import com.djadiv.budgetbackendv3.errors.excepions.UserMessage
import com.djadiv.budgetbackendv3.errors.models.ErrorMessage
import com.djadiv.budgetbackendv3.errors.models.ErrorModel
import org.slf4j.MDC
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import java.util.*
import jakarta.servlet.http.HttpServletRequest
import net.logstash.logback.encoder.org.apache.commons.lang.exception.ExceptionUtils
import mu.KotlinLogging
import net.logstash.logback.argument.StructuredArguments.keyValue

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
class CommonControllerAdvice(
    @Value("\${rest.error.detail.show:false}")
    private val showDetail: Boolean,
) {

    private val logger = KotlinLogging.logger {}


    @ExceptionHandler(Exception::class)
    fun handleExceptions(req: HttpServletRequest, exception: Exception): ResponseEntity<ErrorModel> {
        return when (exception) {
            is HttpMessageNotReadableException -> handleBadRequest(req, exception)
//            is AuthenticationException -> handleAuthRequest(req, exception)
//            is JwtException -> handleAuthRequest(req, exception)
//            is ESignException -> handleESignExceptionRequest(req, exception)
//            is ResetPasswordRequiredException -> handlePasswordResetRequiredException(req, exception)
            is FileUploadException -> handleFIleUPloadException(req, exception)
            is CommonException -> handleCommonException(req, exception)
            else -> handleUnknownException(req, exception)
        }
    }

    private fun handleFIleUPloadException(
        req: HttpServletRequest,
        exception: FileUploadException
    ): ResponseEntity<ErrorModel> {
        val id = UUID.randomUUID();
        logError(exception)
        val error = ErrorModel(
            id = id,
            status = Error.FILE_UPLOAD_ERROR.httpStatus,
            message = Error.FILE_UPLOAD_ERROR.text,
            code = Error.FILE_UPLOAD_ERROR,
            detail = getDetail(exception)
        )
        return ResponseEntity(error, error.status)
    }


    private fun handleBadRequest(req: HttpServletRequest, exception: Exception): ResponseEntity<ErrorModel> {
        val id = UUID.randomUUID()
        logError(exception, id)
        val error = ErrorModel(
            id = id,
            status = Error.BAD_REQUEST.httpStatus,
            error = Error.BAD_REQUEST.text,
            code = Error.BAD_REQUEST,
            detail = getDetail(exception)
        )
        return ResponseEntity(error, error.status)
    }

    private fun handleSignUpRequest(req: HttpServletRequest, exception: Exception): ResponseEntity<ErrorModel> {
        val id = UUID.randomUUID()
        logError(exception, id)
        val error = ErrorModel(
            id = id,
            status = Error.BAD_REQUEST.httpStatus,
            error = exception.message,
            code = Error.BAD_REQUEST,
            detail = getDetail(exception)
        )
        return ResponseEntity(error, error.status)
    }

    private fun handleAuthRequest(req: HttpServletRequest, exception: Exception): ResponseEntity<ErrorModel> {
        val id = UUID.randomUUID()
        logError(exception, id)
        val error = ErrorModel(
            id = id,
            status = Error.AUTHENTICATION_ERROR.httpStatus,
            error = exception.message,
            code = Error.AUTHENTICATION_ERROR,
            detail = getDetail(exception)
        )
        return ResponseEntity(error, error.status)
    }

    private fun handleESignExceptionRequest(req: HttpServletRequest, exception: Exception): ResponseEntity<ErrorModel> {
        val id = UUID.randomUUID()
        logError(exception, id)
        val error = ErrorModel(
            id = id,
            status = Error.E_SIGN_ERROR.httpStatus,
            error = exception.message,
            code = Error.E_SIGN_ERROR,
            detail = getDetail(exception)
        )
        return ResponseEntity(error, error.status)
    }

//    private fun handlePasswordResetRequiredException(
//        req: HttpServletRequest,
//        exception: ResetPasswordRequiredException
//    ): ResponseEntity<ErrorModel> {
//        val id = UUID.randomUUID()
//        val error = ErrorModel(
//            id = id,
//            status = exception.error.httpStatus,
//            error = exception.message,
//            code = exception.error,
//            detail = getDetail(exception),
//            message = getUserMessage(exception.userMessage)
//        )
//        val headers = HttpHeaders()
//        headers.add("reset-password-token", exception.resetPasswordToken)
//        exception.username?.let { headers.add("username", it) }
//        return ResponseEntity(error, headers, error.status)
//    }

    private fun handleUnknownException(req: HttpServletRequest, exception: Exception): ResponseEntity<ErrorModel> {
        val id = UUID.randomUUID()
        logError(exception, id)
        val error = ErrorModel(
            id = id,
            status = Error.UNKNOWN_ERROR.httpStatus,
            error = Error.UNKNOWN_ERROR.text,
            code = Error.UNKNOWN_ERROR,
            detail = getDetail(exception)
        )
        return ResponseEntity(error, error.status)
    }

    private fun handleCommonException(req: HttpServletRequest, ex: CommonException): ResponseEntity<ErrorModel> {
        if (ex.message?.contains(Error.CODE_INVALID.text)?.not()!!) {
            logError(ex)
        }
        val error = ErrorModel(
            id = ex.id,
            status = ex.error.httpStatus,
            error = ex.error.text,
            code = ex.error,
            data = ex.data,
            detail = getDetail(ex),
            message = ex.userMessage
        )
        return ResponseEntity(error, error.status)
    }

    private fun getUserMessage(userMessage: UserMessage?): ErrorMessage? {
        if (userMessage != null)
            return ErrorMessage(messageRu = userMessage.mess)
        else
            return null
    }

    private fun getDetail(ex: Exception) =
        if (showDetail) "Error: ${ex.message}\n" +
                "StackTrace: ${ExceptionUtils.getStackTrace(ex)}"
        else ""

    private fun logError(ex: CommonException) {
        MDC.put("requestId", ex.id.toString())
        MDC.put("code", ex.error.name)
        logger.error(ex.message, keyValue("id", ex.id.toString()), keyValue("code", ex.error.name), ex)
        MDC.clear()
    }

    private fun logError(ex: Exception, id: UUID) {
        MDC.put("requestId", id.toString())
        MDC.put("code", Error.UNKNOWN_ERROR.name)
        logger.error(ex.message, keyValue("id", id.toString()), keyValue("code", Error.UNKNOWN_ERROR.name), ex)
        MDC.clear()
    }


}