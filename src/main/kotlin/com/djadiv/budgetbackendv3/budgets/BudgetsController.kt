package com.djadiv.budgetbackendv3.budgets

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/budgets")
class BudgetsController(
    private val repository: BudgetRepository
) {


    @GetMapping
    fun findAll(): MutableList<BudgetEntity> {
       return repository.findAll()

//        budgets.forEach {
//            println(it.period?.name)
//        }


    }
}