package com.djadiv.budgetbackendv3.budgets

import com.djadiv.budgetbackendv3.budgets.BudgetEntity
import com.djadiv.budgetbackendv3.categories.CategoryEntity
import java.math.BigDecimal
import java.util.*
import jakarta.persistence.*

@Entity
@Table(name = "future_outcomes")
data class FutureOutcomeEntity(
    @Id
    val id: UUID = UUID.randomUUID(),

    @Column(name = "budget_id")
    val budgetId: UUID,

    @ManyToOne
    @JoinColumn(name = "budget_id", insertable = false, updatable = false)
    val budget: BudgetEntity? = null,

    @Column(name = "category_id")
    val categoryId: UUID,

    @ManyToOne
    @JoinColumn(name = "category_id", insertable = false,  updatable = false)
    val category: CategoryEntity? = null,

    val amount: BigDecimal,

    val comment: String? = null,

    val isBuffer: Boolean = false

)
