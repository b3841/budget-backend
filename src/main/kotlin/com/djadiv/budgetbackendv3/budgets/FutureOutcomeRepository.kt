package com.djadiv.budgetbackendv3.budgets

import com.djadiv.budgetbackendv3.budgets.FutureOutcomeEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface FutureOutcomeRepository : JpaRepository<FutureOutcomeEntity, UUID>{
}