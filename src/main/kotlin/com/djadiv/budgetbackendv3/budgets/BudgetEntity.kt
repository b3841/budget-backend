package com.djadiv.budgetbackendv3.budgets

import com.djadiv.budgetbackendv3.periods.PeriodEntity
import java.math.BigDecimal
import java.util.*
import jakarta.persistence.*

@Entity
@Table(name = "budgets")
data class BudgetEntity(

    @Id
    val id: UUID = UUID.randomUUID(),

    @Column(name = "period_id")
    val periodId: UUID,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "period_id", insertable = false, updatable = false)
    val period: PeriodEntity? = null,

    val incomeSum: BigDecimal, // планируемая сумма доходов

    val comment: String? = null
)