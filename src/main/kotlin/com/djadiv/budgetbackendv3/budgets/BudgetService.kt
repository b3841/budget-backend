package com.djadiv.budgetbackendv3.budgets

import com.djadiv.budgetbackendv3.budgets.BudgetRepository
import org.springframework.stereotype.Service
import java.util.*

@Service
class BudgetService(
    private val repository: BudgetRepository
) {
    fun findAllByPeriodId(id: UUID) =
        repository.findAllByPeriodId(id)

}