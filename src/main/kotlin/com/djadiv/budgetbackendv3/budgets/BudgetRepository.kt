package com.djadiv.budgetbackendv3.budgets

import com.djadiv.budgetbackendv3.budgets.BudgetEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface BudgetRepository : JpaRepository<BudgetEntity, UUID> {

    fun findAllByPeriodId(id: UUID): List<BudgetEntity>

//    @EntityGraph(
//        type = EntityGraph.EntityGraphType.FETCH,
//        attributePaths = ["incomeSum"]
//    )
//    override fun findAll(): List<BudgetEntity>

}