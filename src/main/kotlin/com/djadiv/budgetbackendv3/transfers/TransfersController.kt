package com.djadiv.budgetbackendv3.transfers

import com.djadiv.budgetbackendv3.transfers.dto.CreateTransferDTO
import com.djadiv.budgetbackendv3.transfers.dto.TransferTableDTO
import com.djadiv.budgetbackendv3.transfers.dto.TransfersFilterDTO
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("/api/private/v1/transfers")
class TransfersController(
    private val service: TransfersService
) {

    @PostMapping
    fun table(
        @RequestBody filter: TransfersFilterDTO
    ): List<TransferTableDTO> {
        println(filter)
        return service.findAll(filter)
    }

    @GetMapping("/{id}")
    fun getOne(@PathVariable id: UUID): CreateTransferDTO {
        return service.findById(id)
    }

    @PostMapping("/save")
    fun save(@RequestBody createTransfer: CreateTransferDTO) {
        if (createTransfer.id == null)
            service.create(createTransfer)
        else
            service.update(createTransfer)
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: UUID) {
        service.delete(id)
    }

}