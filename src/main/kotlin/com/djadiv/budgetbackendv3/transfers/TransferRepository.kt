package com.djadiv.budgetbackendv3.transfers

import com.djadiv.budgetbackendv3.movements.MovementEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.time.LocalDateTime
import java.util.*

@Repository
interface TransferRepository : JpaRepository<TransferEntity, UUID> {

    @Query(
        """SELECT a FROM TransferEntity a
            WHERE (CAST (:startDate AS org.hibernate.type.LocalDateTimeType) IS NULL OR a.date >= :startDate)
            AND (CAST (:endDate AS org.hibernate.type.LocalDateTimeType) IS NULL OR a.date <= :endDate)
            AND (CAST(:periodId AS org.hibernate.type.UUIDCharType) IS NULL OR a.sourceMovement.periodId = :periodId)
            AND (CAST(:sourceAccountId AS org.hibernate.type.UUIDCharType) IS NULL OR a.sourceMovement.accountId = :sourceAccountId)
            AND (CAST(:targetAccountId AS org.hibernate.type.UUIDCharType) IS NULL OR a.targetMovement.accountId = :targetAccountId)
            ORDER BY date DESC
        """
    )
    fun findByFilters(
        startDate: LocalDateTime?,
        endDate: LocalDateTime?,
        periodId: UUID?,
        sourceAccountId: UUID?,
        targetAccountId: UUID?,
    ): List<TransferEntity>


    @Query(
        """
        SELECT DISTINCT a FROM TransferEntity a
        WHERE a.targetMovement.id IN (:mvmIds)
        OR a.sourceMovement.id IN (:mvmIds)
        OR a.fee.id IN (:mvmIds)
    """
    )
    fun findByMovementIds(mvmIds: List<UUID>): List<TransferEntity>


    @Query(
        """
            SELECT a FROM TransferEntity a
            WHERE a.fee IN (:fees)
        """
    )
    fun findByTransferFees(fees: List<MovementEntity>): List<TransferEntity>

}