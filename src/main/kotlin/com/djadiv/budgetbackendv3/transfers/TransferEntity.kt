package com.djadiv.budgetbackendv3.transfers

import com.djadiv.budgetbackendv3.periods.PeriodEntity
import com.djadiv.budgetbackendv3.movements.MovementEntity
import com.djadiv.budgetbackendv3.transfers.dto.CreateTransferDTO
import com.djadiv.budgetbackendv3.transfers.dto.TransferTableDTO
import java.math.BigDecimal
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.util.*
import jakarta.persistence.*

@Entity
@Table(name = "transfers")
data class TransferEntity(

    @Id
    val id: UUID = UUID.randomUUID(),

    @Column
    val sourceAccountName: String,

    @Column
    val targetAccountName: String,

    @ManyToOne
    @JoinColumn(name = "period_id")
    val period: PeriodEntity,

    @Column(name = "m_date")
    val date: LocalDateTime,

    @Column(name = "operation_amount")
    val operationAmount: BigDecimal = BigDecimal.ZERO,

    val comment: String? = null,

    @OneToOne
    @JoinColumn(name = "source_id"/*, insertable = false, updatable = false*/)
    val sourceMovement: MovementEntity,

    @OneToOne
    @JoinColumn(name = "target_id"/*, updatable = false, insertable = false*/)
    val targetMovement: MovementEntity,

    @OneToOne
    @JoinColumn(name = "fee_id"/*, updatable = false, insertable = false*/)
    val fee: MovementEntity? = null

) {
    fun toTableDto() = TransferTableDTO(
        id = id,
        sourceAccount = sourceAccountName,
        sourceAccountBalanceBefore = sourceMovement.balance + sourceMovement.operationAmount.abs(),
        sourceAccountBalanceAfter = sourceMovement.balance,
        targetAccount = targetAccountName,
        targetAccountBalanceBefore = targetMovement.balance - targetMovement.operationAmount,
        targetAccountBalanceAfter = targetMovement.balance,
        date = LocalDate.from(date),
        amount = operationAmount,
        comment = comment,
        fee = fee?.operationAmount?.abs()
    )

    fun toCreateDto() = CreateTransferDTO(
        id = id,
        sourceAccountId = sourceMovement.accountId,
        targetAccountId = targetMovement.accountId,
        date = LocalDate.from(sourceMovement.date),
        time = LocalTime.from(sourceMovement.date),
        amount = operationAmount,
        comment = comment,
        fee = fee?.operationAmount?.abs()
    )

}
