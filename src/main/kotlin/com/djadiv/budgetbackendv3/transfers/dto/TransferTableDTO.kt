package com.djadiv.budgetbackendv3.transfers.dto

import java.math.BigDecimal
import java.time.LocalDate
import java.util.*

data class TransferTableDTO(
    var id: UUID,
    var sourceAccount: String,
    var sourceAccountBalanceBefore: BigDecimal,
    var sourceAccountBalanceAfter: BigDecimal,
    var targetAccount: String,
    var targetAccountBalanceBefore: BigDecimal,
    var targetAccountBalanceAfter: BigDecimal,
    var date: LocalDate,
    var amount: BigDecimal,
    var comment: String? = null,
    var fee: BigDecimal? = null
)