package com.djadiv.budgetbackendv3.transfers.dto

import com.djadiv.budgetbackendv3.enums.RangeTypeEnum
import java.time.LocalDate
import java.util.*

data class TransfersFilterDTO(
    val rangeType: RangeTypeEnum,
    val startDate: LocalDate? = null,
    val endDate: LocalDate? = null,
    val periodId: UUID?,
    val accountId: UUID?,
    var sourceAccountId: UUID? = null,
    var targetAccountId: UUID? = null
)