package com.djadiv.budgetbackendv3.transfers.dto

interface TransferShortDTO {
    val movementId: String
    val isSender: Boolean
    val targetAccount: String?
    val isReceiver: Boolean
    val sourceAccount: String?
}