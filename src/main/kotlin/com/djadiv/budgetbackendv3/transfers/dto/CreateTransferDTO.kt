package com.djadiv.budgetbackendv3.transfers.dto

import java.math.BigDecimal
import java.time.LocalDate
import java.time.LocalTime
import java.util.*
import jakarta.validation.constraints.NotEmpty

data class CreateTransferDTO(
    var id: UUID? = null,

    @field:NotEmpty(message = "Укажите счет")
    var sourceAccountId: UUID,

    @field:NotEmpty(message = "Укажите счет")
    var targetAccountId: UUID,

    @field:NotEmpty(message = "Укажите дату")
    val date: LocalDate,

    @field:NotEmpty(message = "Укажите время")
    val time: LocalTime,

    @field:NotEmpty(message = "Укажите сумму")
    val amount: BigDecimal,

    val comment: String? = null,

    var fee: BigDecimal? = null
    )