package com.djadiv.budgetbackendv3.transfers

import com.djadiv.budgetbackendv3.accounts.AccountRepository
import com.djadiv.budgetbackendv3.accounts.AccountEntity
import com.djadiv.budgetbackendv3.periods.PeriodEntity
import com.djadiv.budgetbackendv3.enums.MovementTypeEnum
import com.djadiv.budgetbackendv3.enums.RangeTypeEnum
import com.djadiv.budgetbackendv3.errors.excepions.IllegalArgumentException
import com.djadiv.budgetbackendv3.movements.MovementEntity
import com.djadiv.budgetbackendv3.movements.MovementService
import com.djadiv.budgetbackendv3.movements.dto.CreateMovementDTO
import com.djadiv.budgetbackendv3.periods.PeriodsService
import com.djadiv.budgetbackendv3.settings.SettingsKey
import com.djadiv.budgetbackendv3.settings.SettingsService
import com.djadiv.budgetbackendv3.transfers.dto.CreateTransferDTO
import com.djadiv.budgetbackendv3.transfers.dto.TransferTableDTO
import com.djadiv.budgetbackendv3.transfers.dto.TransfersFilterDTO
import com.djadiv.budgetbackendv3.utils.extractDates
import com.djadiv.budgetbackendv3.utils.formatWithYear
import com.djadiv.budgetbackendv3.utils.toLocalDateTimeEnd
import com.djadiv.budgetbackendv3.utils.toLocalDateTimeStart
import org.junit.jupiter.api.Assertions
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.math.BigDecimal
import java.time.LocalDateTime
import java.util.*

@Service
class TransfersService(
    private val repository: TransferRepository,
    private val movementService: MovementService,
    private val settingsService: SettingsService,
    private val periodsService: PeriodsService,
    private val accountRepository: AccountRepository,

    ) {
    fun findAll(filter: TransfersFilterDTO): List<TransferTableDTO> {
        val (startDate, endDate, periodId) = extractDates(
            filter.startDate, filter.endDate, filter.rangeType, filter.periodId
        )
        
        return repository
            .findByFilters(startDate, endDate, periodId, filter.sourceAccountId, filter.targetAccountId)
            .map { it.toTableDto() }
    }

    fun findById(id: UUID): CreateTransferDTO {
        return repository.findByIdOrNull(id)?.toCreateDto()
            ?: throw IllegalArgumentException("Transfer with id $id not found")
    }

    @Transactional
    fun create(createTransfer: CreateTransferDTO): TransferEntity {

        val validate = validate(createTransfer)

        // создать расход
        val outcome = movementService.create(createOutcome(validate, createTransfer))

        // создать приход
        val income = movementService.create(createIncome(validate, createTransfer))

        // создать комиссию
        val feeEntity = createTransfer.fee
            ?.takeIf { it != BigDecimal.ZERO }
            ?.let { movementService.create(createFee(validate, createTransfer, it)) }

        val transferEntity = TransferEntity(
            sourceAccountName = validate.sourceAccount.name,
            targetAccountName = validate.targetAccount.name,
            period = validate.period,
            date = validate.date,
            operationAmount = createTransfer.amount,
            comment = createTransfer.comment,
            sourceMovement = outcome,
            targetMovement = income,
            fee = feeEntity
        )

        return repository.save(transferEntity)
    }

    @Transactional
    fun update(createTransfer: CreateTransferDTO) {

        val validate = validate(createTransfer)

        var existingTransfer: TransferEntity = repository.findByIdOrNull(createTransfer.id)
            ?: throw IllegalArgumentException("No transfer with [id: ${createTransfer.id}]")
        // TODO (isTransfer prop must not be changed)
        val outcome = movementService.update(createOutcome(validate, createTransfer)
            .also { it.id = existingTransfer.sourceMovement.id })

        val income = movementService.update(createIncome(validate, createTransfer)
            .also { it.id = existingTransfer.targetMovement.id })

        var feeEntity: MovementEntity? = null
        createTransfer.fee?.let { feeAmount ->
            if (feeAmount == BigDecimal.ZERO)
                existingTransfer.fee?.let { movementService.delete(existingTransfer.fee!!.id) }
            else {
                val createFee = createFee(validate, createTransfer, feeAmount, existingTransfer)

                feeEntity = existingTransfer.fee
                    ?.let { movementService.update(createFee) }
                    ?: movementService.create(createFee)
            }
        }

        repository.save(
            existingTransfer.copy(
                sourceAccountName = validate.sourceAccount.name,
                targetAccountName = validate.targetAccount.name,
                period = validate.period,
                date = validate.date,
                operationAmount = createTransfer.amount,
                comment = createTransfer.comment,
                sourceMovement = outcome,
                targetMovement = income,
                fee = feeEntity
            )
        )
    }

    private fun validate(createTransfer: CreateTransferDTO): ValidateDTO {
        val sourceAccountId = createTransfer.sourceAccountId
        val targetAccountId = createTransfer.targetAccountId
        val date = LocalDateTime.of(createTransfer.date, createTransfer.time)

        val sourceAccount = accountRepository.findByIdOrNull(sourceAccountId)
            ?: throw IllegalArgumentException("No source account with [id: $sourceAccountId]")

        val targetAccount = accountRepository.findByIdOrNull(targetAccountId)
            ?: throw IllegalArgumentException("No target account with [id: $targetAccountId]")

        if (sourceAccountId == targetAccountId)
            throw IllegalArgumentException(
                "Source and target accounts are equal [id: $sourceAccountId]",
                "Исходный и целевой счет одинаковые"
            )

        val period = periodsService.periodByDate(date)
            ?: throw IllegalArgumentException(
                "There is no period with date $date",
                "Указанная дата ${date.formatWithYear()} не входит ни в один период"
            )

        val senderCategoryId = settingsService.findByKey(SettingsKey.TRANSFER_SENDER_CATEGORY_ID)
        val receiverCategoryId = settingsService.findByKey(SettingsKey.TRANSFER_RECEIVER_CATEGORY_ID)

        return ValidateDTO(
            sourceAccountId,
            targetAccountId,
            date,
            sourceAccount,
            targetAccount,
            period,
            senderCategoryId,
            receiverCategoryId
        )
    }

    private fun createIncome(
        validate: ValidateDTO,
        createTransfer: CreateTransferDTO
    ): CreateMovementDTO {
        val comment = createTransfer.comment?.let { " ($it)" }?: ""
        return CreateMovementDTO(
            categoryId = validate.receiverCategoryId,
            accountId = validate.targetAccountId,
            date = createTransfer.date,
            time = createTransfer.time,
            amount = createTransfer.amount,
            comment = "Перевод c ${validate.sourceAccount.name}$comment",
            type = MovementTypeEnum.INCOME,
            isTransfer = true
        )
    }
    private fun createOutcome(
        validate: ValidateDTO,
        createTransfer: CreateTransferDTO
    ): CreateMovementDTO {
        val comment = createTransfer.comment?.let { " ($it)" }?: ""
       return  CreateMovementDTO(
            categoryId = validate.senderCategoryId,
            accountId = validate.sourceAccountId,
            date = createTransfer.date,
            time = createTransfer.time,
            amount = createTransfer.amount,
            comment = "Перевод на ${validate.targetAccount.name}$comment",
            type = MovementTypeEnum.OUTCOME,
            isTransfer = true
        )
    }

    private fun createFee(
        validate: ValidateDTO,
        createTransfer: CreateTransferDTO,
        feeAmount: BigDecimal,
        existingTransfer: TransferEntity? = null
    ): CreateMovementDTO {

        val existingFee = existingTransfer?.fee
        val comment = createTransfer.comment?.let { " ($it)" } ?: ""

        // если обновляется комиссия
        // если время перевода изменилось, то новое время комиссии +1с
        // если время не изменилось, то брать время из входящих аргументов
        val newFee = if (existingFee != null) {
            val isTimeChanged = existingTransfer.date == LocalDateTime.of(createTransfer.date, createTransfer.time)
            val newTime = if (isTimeChanged) createTransfer.time.plusSeconds(1) else createTransfer.time

            CreateMovementDTO(
                id = existingTransfer.fee.id,
                categoryId = existingFee.categoryId,
                accountId = validate.sourceAccountId,
                date = createTransfer.date,
                time = newTime,
                amount = feeAmount,
                comment = "Комиссия за перевод на ${validate.targetAccount.name}$comment",
                type = MovementTypeEnum.OUTCOME
            )
        } else // если создается комиссия, то ее время +1с
            CreateMovementDTO(
                categoryId = settingsService.findByKey(SettingsKey.FEE_CATEGORY_ID),
                accountId = validate.sourceAccountId,
                date = createTransfer.date,
                time = createTransfer.time.plusSeconds(1),
                amount = feeAmount,
                comment = "Комиссия за перевод на ${validate.targetAccount.name}$comment",
                type = MovementTypeEnum.OUTCOME
            )

        return newFee
    }

    @Transactional
    fun delete(id: UUID) {
        var existingTransfer: TransferEntity = repository.findByIdOrNull(id)
            ?: throw IllegalArgumentException("No transfer with [id: $id]")
        repository.deleteById(id)

        movementService.delete(existingTransfer.sourceMovement.id)
        movementService.delete(existingTransfer.targetMovement.id)

    }


}

data class ValidateDTO(
    val sourceAccountId: UUID,
    val targetAccountId: UUID,
    val date: LocalDateTime,
    val sourceAccount: AccountEntity,
    val targetAccount: AccountEntity,
    val period: PeriodEntity,
    val senderCategoryId: String, // fа что они здесь делают??
    val receiverCategoryId: String
)

