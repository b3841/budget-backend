package com.djadiv.budgetbackendv3.components

import org.springframework.boot.availability.AvailabilityChangeEvent
import org.springframework.boot.availability.ReadinessState
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component

@Component
class MyReadinessStateExporter {

    @EventListener
    fun onStateChange(event: AvailabilityChangeEvent<ReadinessState>){
        when(event.state){
            ReadinessState.ACCEPTING_TRAFFIC -> print("---Ready to accept traffic")
            ReadinessState.REFUSING_TRAFFIC -> print("---Refuse all traffic")
        }
    }

}