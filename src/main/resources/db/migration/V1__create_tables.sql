CREATE TABLE IF NOT EXISTS accounts
(
    id         UUID PRIMARY KEY,
    name       VARCHAR    NOT NULL,
    currency   VARCHAR(3) NOT NULL,
    is_savings BOOLEAN    NOT NULL DEFAULT FALSE,
    hidden     BOOLEAN    NOT NULL DEFAULT FALSE
);

CREATE TABLE IF NOT EXISTS categories
(
    id        varchar PRIMARY KEY,
    name      VARCHAR NOT NULL,
    parent_id varchar
        CONSTRAINT categories_parent_fk REFERENCES categories (id) DEFAULT NULL,
    is_storno BOOLEAN NOT NULL                                     DEFAULT FALSE,
    type      VARCHAR NOT NULL                                     DEFAULT 'OUTCOME'
);

CREATE TABLE IF NOT EXISTS periods
(
    id         UUID PRIMARY KEY,
    name       VARCHAR   NOT NULL,
    start_date TIMESTAMP NOT NULL,
    end_date   TIMESTAMP,
    comment    VARCHAR
);

CREATE TABLE IF NOT EXISTS movements
(
    id               UUID PRIMARY KEY,
    category_id      VARCHAR       NOT NULL
        CONSTRAINT category_fk REFERENCES categories (id),
    account_id       UUID          NOT NULL
        CONSTRAINT account_fk REFERENCES accounts (id),
    period_id        UUID          NOT NULL
        CONSTRAINT period_fk REFERENCES periods (id),
    m_date           TIMESTAMP     NOT NULL,
    operation_amount NUMERIC(9, 2) NOT NULL,
    comment          VARCHAR,
    balance          NUMERIC(9, 2) NOT NULL,
    type             VARCHAR       NOT NULL DEFAULT 'OUTCOME', -- INCOME | OUTCOME
    is_transfer      BOOLEAN       NOT NULL DEFAULT FALSE,
    store            VARCHAR,
    receipt_id       UUID
        CONSTRAINT receipt_fk REFERENCES receipts (id),
    file_ids         TEXT
-- UNCOMMENT IF NO BULK INSERT REQUIRED
-- ,
--     CONSTRAINT movements_account_n_date_unique
--         UNIQUE (account_id, m_date)
);
-- UNCOMMENT IF BULK INSERT REQUIRED AFTER ALL THE INSERTS ARE MADE
-- CREATE UNIQUE INDEX movements_account_n_date_unique ON movements
--     (account_id, m_date);

CREATE TABLE IF NOT EXISTS transfers
(
    id                  UUID PRIMARY KEY,
    source_account_name varchar       NOT NULL,
    target_account_name varchar       NOT NULL,
    m_date              TIMESTAMP     NOT NULL UNIQUE,
    operation_amount    NUMERIC(9, 2) NOT NULL,
    comment             VARCHAR,
    period_id           UUID          NOT NULL
        CONSTRAINT period_fk REFERENCES periods (id),
    source_id           UUID          NOT NULL
        CONSTRAINT transfers_source_fk REFERENCES movements (id),
    target_id           UUID          NOT NULL
        CONSTRAINT transfers_target_fk REFERENCES movements (id),
    fee_id              UUID
        CONSTRAINT transfers_fee_fk REFERENCES movements (id)
);

CREATE TABLE IF NOT EXISTS budgets
(
    id         UUID PRIMARY KEY,
    period_id  UUID
        CONSTRAINT budgets_period_fk REFERENCES periods (id),
    income_sum NUMERIC(9, 2),
    comment    VARCHAR
);

CREATE TABLE IF NOT EXISTS future_outcomes
(
    id          UUID PRIMARY KEY,
    budget_id   UUID          NOT NULL
        CONSTRAINT future_outcomes_budget_fk REFERENCES budgets (id),
    category_id VARCHAR       NOT NULL
        CONSTRAINT future_outcomes_category_fk REFERENCES categories (id),
    amount      NUMERIC(9, 2) NOT NULL,
    comment     VARCHAR,
    is_buffer   BOOLEAN       NOT NULL DEFAULT FALSE
);

CREATE TABLE IF NOT EXISTS settings
(
    id    serial PRIMARY KEY,
    key   varchar NOT NULL,
    value varchar NOT NULL
);

-- Table for dev purposes only. TODO Store to cloud!
CREATE TABLE IF NOT EXISTS movement_files
(
    file_id   varchar NOT NULL
        constraint movement_files_pk
            primary key,
    file_name varchar NOT NULL,
    data      bytea   NOT NULL
);

CREATE TABLE IF NOT EXISTS receipts
(
    id        UUID PRIMARY KEY,
    amount    NUMERIC(9, 2) NOT NULL,
    date      TIMESTAMP     NOT NULL,
    period_id UUID          NOT NULL
        CONSTRAINT period_fk REFERENCES periods (id),
    comment   VARCHAR,
    location  VARCHAR,
    file_ids  TEXT
);

CREATE UNIQUE INDEX receipts_date_uindex
    ON receipts (date);




