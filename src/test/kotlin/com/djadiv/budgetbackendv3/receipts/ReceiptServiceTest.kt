package com.djadiv.budgetbackendv3.receipts

import com.djadiv.budgetbackendv3.enums.MovementTypeEnum
import com.djadiv.budgetbackendv3.movements.MovementService
import com.djadiv.budgetbackendv3.movements.dto.CreateMovementDTO
import io.mockk.mockk
import io.mockk.verify
import org.hibernate.query.sqm.TemporalUnit
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.math.BigDecimal
import java.time.LocalDate
import java.time.LocalTime
import java.util.UUID

@RunWith(MockitoJUnitRunner::class)
class ReceiptServiceTest {
//    val repository: ReceiptRepository = mockk(relaxed = true)
//    val validator: ReceiptCreateValidator = mockk(relaxed = true)
//    val movementService: MovementService = mockk(relaxed = true)

    @Mock
    lateinit var  repository: ReceiptRepository
    val validator: ReceiptCreateValidator = mockk(relaxed = true)
    val movementService: MovementService = mockk(relaxed = true)

    @InjectMocks
    var receiptService: ReceiptService = ReceiptService(
        repository, validator, movementService
    )

    val PRODUCTY_CAT_ID = "08a4181d-ab0e-4751-a1ce-297eccbf639e"
    val HYGIENE_CAT_ID = "df551947-52f8-40fb-983f-1a1241889554"
    val KASPI_ID = UUID.fromString("297ad74a-8949-4be6-957e-6a7431779ca6")
    val date = LocalDate.of(2023, 8, 30)
    var time = LocalTime.of(23, 21, 21)

    fun `store receipt with 1 movement after all movements`() {

        val receipt = CreateReceiptDTO(
            id = null,
            date = date,
            time = time,
            comment = "Тестовый чек",
            movements = listOf(
                CreateMovementDTO(
                    categoryId = PRODUCTY_CAT_ID, // Продукты
                    accountId = KASPI_ID,
                    date = date,
                    time = time,
                    amount = BigDecimal(1000),
                    type = MovementTypeEnum.OUTCOME,
                    comment = "Хлеб"
                ),
                CreateMovementDTO(
                    categoryId = PRODUCTY_CAT_ID, // Продукты
                    accountId = KASPI_ID,
                    date = date,
                    time = time.plusSeconds(1),
                    amount = BigDecimal(2300),
                    type = MovementTypeEnum.OUTCOME,
                    comment = "Молоко"
                )
            )
        )
        
        receiptService.create(receipt)
        
        verify ( exactly = 1  ){}
        
    }
}












