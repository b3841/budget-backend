package com.djadiv.budgetbackendv3.accounts


import com.djadiv.budgetbackendv3.movements.MovementService
import io.mockk.mockk
import io.mockk.verify
import org.springframework.boot.test.context.SpringBootTest
import org.junit.jupiter.api.Test
//@SpringBootTest
internal class AccountServiceTest {
    
    val accountRepository: AccountRepository = mockk(relaxed = true)
    val movementService: MovementService = mockk(relaxed = true)
    val accountService2: AccountService2 = mockk(relaxed = true)
    
    private var accountService: AccountService = AccountService(
        accountRepository,
        movementService,
        accountService2
    )

    @Test
    fun `should find all`(){
        accountService.findAll(onlyDictionary = false)
        
        verify ( exactly = 1 ) {accountRepository.findAll()}
    }

}