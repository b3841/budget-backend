# download and cache all dependencies
FROM gradle:7.6.1-jdk11@sha256:277d257f9eca0c321e7a9e898d55b975b48120ff1326f1082935b932b73054e3 AS cache
RUN mkdir -p /home/gradle/cache_home
ENV GRADLE_USER_HOME /home/gradle/cache_home
COPY build.gradle.kts /home/gradle/java-code/
WORKDIR /home/gradle/java-code
RUN gradle clean build -i --stacktrace -x bootJar

# build backend.jar
FROM gradle:7.6.1-jdk11@sha256:277d257f9eca0c321e7a9e898d55b975b48120ff1326f1082935b932b73054e3 AS builder
COPY --from=cache /home/gradle/cache_home /home/gradle/.gradle
COPY . /usr/src/java-code/
WORKDIR /usr/src/java-code
RUN gradle bootJar -i --stacktrace

# run java -jar
FROM amazoncorretto:11.0.19@sha256:c76c2900e690191516a9dec2cf886d699cff1b4988dad320021e9135f33d4a15
EXPOSE 8082
USER root
WORKDIR /usr/src/java-app
COPY --from=builder /usr/src/java-code/build/libs/*.jar ./backend.jar
ENTRYPOINT exec java ${JVM_OPTS} -Djava.security.egd=file:/dev/./urandom -Dlog4j2.formatMsgNoLookups=true -jar /usr/src/java-app/backend.jar \
                      --spring.profiles.active=\${ACTIVE_PROFILE} \
                      --datasource-host=\${DATASOURCE_URL} \
                      --datasource-user=\${DATASOURCE_USER} \
                      --datasource-pass=\${DATASOURCE_PASS}








#FROM openjdk:11-jre-slim
