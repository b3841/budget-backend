# Budget Project

Система состоит бэкенда Kotlin Spring Boot, фронтенда Angular и БД PostgreSQL.
Java v 11


#### PostgreSQL
 1. Перед запуском бэкенда (любым нижеописанным способом), нужно сначала запустить контейнер с БД. В терминале из любой папки
    1. `docker run --name postgres -e POSTGRES_PASSWORD=postgres -p 5432:5432 -d postgres`
    2. Docker загружает образ postgres (если не найдет локально) и запускает его в контейнере,
       устанавливая имя контейнера "postgres" (`--name postgres`). Открывает порт и устанавливает пароль по-умолчанию
       "postgres"
 2. Зайти "внутрь" только запущенного контейнера. В терминале из любой папки:
    1. `docker exec -it postgres bash`
    2. Docker открывает оболочку bash операционной системы внутри контейнера, указанного после `-it` (`postgres`)
 3. Внутри этой оболочки выполнить:
    1. `psql -U postgres` - подключение через утилиту psql к базе под юзером postgres
    2. `CREATE DATABASE budget_v3;`- создать новую БД

### Запуск бэкенда
Можно запустить одним из способов:

#### 1. Запуск в терминале (No Docker)
 * В терминале в корневой директории выполнить 
   * `.\gradlew bootRun`
 * Gradle соберет приложение и запустит его. Если БД поднята, то Spring автоматически 
к ней подключится по креденшлам, указанным в `application.properties`
 * После подключения к БД `Flyway` выполняет скрипты из папки `db.migration`.
создавая таблицы. Нужно раскомментировать в файле `V1__create_tables.sql` строку 45, чтобы создался индекс уникальности 
`V1__create_tables.sql`. Если после запуска базы и приложения в таблицу movements нужно загрузить вручную большое 
количество строк (например, миграция из другого приложения), то рекомендуется сначала произвести миграцию данных, 
затем создать индекс вручную, как описано в строке 50.

#### 2. Запуск в Intellij Idea (No Docker)
 * Установить в Project Structure верисию Java не ниже 11.
 * Устновить Kotlin плагины, SDK и остальное, что попросит Idea
 * Run configuration должен сздаться автоматически

#### 3. Запуск в Docker
 * В терминале в корневой директории проекта:
   * `docker build -t backend .`
   * Docker создаст образ из описания в `Dockerfile`, который находится в корне проекта; и даст образу имя "backend"
   * В терминале (можно из любой директории):
     * `docker run -it -d --rm -e ACTIVE_PROFILE=local-docker -p 8082:8082 --link postgres --name backend backend`
   * Docker запускает только что созданный образ "backend" в контейнере (которому так же дает имя "backend"). 
   Параметр `--rm` указывает, что контейнер будет удален после того, как он будет остановлен. Полезно при отладке 
   `Dockerfile`'а, чтобы после неудачных конфигураций не скапливалось много ненужных контейнеров. Если не указывать
   этот параметр, то контейнер после запуска не будет удален, и его можно будет вновь запустить командой 
   `docker start backend`. При этом БД должна быть поднята заранее. Для обычного демо параметр `--rm` можно 
   опустить.
   * В переменные окружения передается имя профиля, с которым Spring Boot должен запуститься `-e ACTIVE_PROFILE=local-docker`.
   Он указывает спрингу на файл `application-local-docker`, в котором url БД описан как 
   `jdbc:postgresql://postgres:5432/budget_v3`. Это позволяет подключаться к БД по имени контейнера, в котором 
   развернута БД (если она конечно развернута в контейнере, а не установлена на хостовой машине). Для этой же цели
    указывается параметр `--link postgres` - чтобы контейнер backend мог обращаться к контейнеру postgres по имени, 
   а не по IP-адресу. В целом связь между двумя контейнерами можно сделать и через `docker network`. Это один из вариантов.
   * Так как Spring приложение запускается на порту 8082, то открывается соответствующий порт у контейнере
   `-p 8082:8082`
   * Логи контейнера можно посмотреть
   `docker logs backend`
   * Успешный старт бэкенда должен сопровождаться последней строкой в логах (сразу после запуска):
     * `INFO 1 --- [           main] c.d.b.BudgetBackendV3ApplicationKt       : Started BudgetBackendV3ApplicationKt in 6.506 seconds (JVM running for 7.233)`

#### 3. Запуск через docker-compose
В корне лежит файл `docker-compose.yml`. В нем описаны три сервиса, который будут подняты после команды 
`docker-compose up`. Инмена для этих контейнеров имеют постфикс `_compose` только в целях удобочитаемости в списках 
контейнеров, получаемых комнадой `docker ps`. Для того, чтобы compose запустился папка с исходниками frontend приложения
должна присутствовать в файловой системе. Пункт `services.frontend.build: ../budget-frontend-v3` полагает, что 
папка с исходниками frontend находится на одном уровне с папкой backend. Если нет, то нужно указать относительный
путь к этой папке.
 * `docker-compose up`

After backend has started all required tables are created automatically with the help of Migration engine.
Files are located under src/resources/db/migration folder. 

If you need to import movements (there may be a lot) UNIQUE INDEX must not be created and is hidden by default
in V1_create_tables.sql file (Marked as "UNCOMMENT IF NO BULK INSERT REQUIRED").
First execute import data to table movements, then create that index 
(marked as "UNCOMMENT IF BULK INSERT REQUIRED..." in V1_create_tables.sql).
If you create that index first, then import a lot of rows at a time, for example 6000 rows, it will lead to performance 
degradation while inserting this. 


